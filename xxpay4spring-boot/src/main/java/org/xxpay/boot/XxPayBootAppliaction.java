package org.xxpay.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 */
@SpringBootApplication
@ComponentScan(basePackages={"org.xxpay"})
public class XxPayBootAppliaction {
    public static void main(String[] args) {
        SpringApplication.run(XxPayBootAppliaction.class, args);
    }
}