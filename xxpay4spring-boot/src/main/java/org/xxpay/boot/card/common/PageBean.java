package org.xxpay.boot.card.common;

import java.util.ArrayList;
import java.util.List;

public class PageBean {
	
	private int total;
	
	private List list = new ArrayList<>();

	public PageBean() {
		super();
	}

	public PageBean(int total, List list) {
		super();
		this.total = total;
		this.list = list;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

}
