package org.xxpay.boot.card.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysUser;

public interface SysUserServive {
	
	List<SysUser> infoSysUser(String loginAccount) throws Exception;
	
	SysUser info(@Param("userId")String userId) throws Exception;
	
	
}
