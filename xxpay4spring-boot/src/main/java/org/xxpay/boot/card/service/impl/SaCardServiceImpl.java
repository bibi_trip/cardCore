package org.xxpay.boot.card.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SaCardServive;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.common.domain.BaseParam;
import org.xxpay.common.util.BeanConvertUtils;
import org.xxpay.common.util.JsonUtil;
import org.xxpay.common.util.RpcUtil;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SysUser;

import com.alibaba.fastjson.JSONObject;

@Service
public class SaCardServiceImpl extends CardBaseService implements SaCardServive{
	
	public List<SaCard> infoSaCard(String userId) throws Exception{
		return super.infoSaCard(userId);
	}
	
	@Override
	public int updateSaCard(String cardNumber,String flag,String remainPart) throws Exception {
		return super.updateSaCard(cardNumber,flag,remainPart);
	}

	@Override
	public int insertSaCard(SaCard saCard) throws Exception {
		return super.insertSaCard(saCard);
	}

	@Override
	public PageBean listSaCard(int start,int end) throws Exception {
		return super.listSaCard(start,end);
	}

	@Override
	public int updateAll(SaCard saCard) throws Exception {
		
		return super.updateAll(saCard);
	}
}
