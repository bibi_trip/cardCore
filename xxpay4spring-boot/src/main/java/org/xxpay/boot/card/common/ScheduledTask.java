package org.xxpay.boot.card.common;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.stereotype.Component;
import org.xxpay.boot.card.core.SaDealRecordCore;
import org.xxpay.boot.card.core.SaScoreSettleBillCore;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;

@EnableScheduling
@Component
public class ScheduledTask{
	
	@Resource
	private SaScoreSettleBillCore saScoreSettleBillCore;
	
	@Resource
	private SaDealRecordCore saDealRecordCore;
	
	@Scheduled(cron="0 10 10 1 * ?")
	public void before(){
		System.out.println("定时任务启动......");
		saScoreSettleBillCore.save(new SysScoreSettleBill());
	}
	
	/***
	 * 
	 * 自动传输数据(测试)
	 * 
	 */
	//@Scheduled(cron="0/10 * *  * * ?")
	public void trys(){
		SaDealRecord saDealRecord = new SaDealRecord();
		saDealRecord.setMerchantOrderNumber("201803012345");
		saDealRecord.setLoginAccount("1736455814");
		saDealRecord.setDealType('1');
		saDealRecord.setProductType('5');
		saDealRecord.setTripTime("2017-12-15 23:00:00");
		saDealRecord.setOrderAmount(Double.valueOf("1364"));
		saDealRecord.setDealDetail("laisuhdflkajsdhlfkjhsajlkdf");
		saDealRecord.setDealCreateTime("2017-12-14 19:23:12");
		saDealRecordCore.save(saDealRecord, null, null);
	}
	
}