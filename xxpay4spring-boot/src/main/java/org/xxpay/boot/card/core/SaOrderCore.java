package org.xxpay.boot.card.core;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.ctrl.SaOrderController;
import org.xxpay.boot.card.service.SaCardServive;
import org.xxpay.boot.card.service.SysProxyUserServive;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SysProxyUser;
import org.xxpay.dal.dao.card.model.SysUser;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 订单管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/saOrder")
public class SaOrderCore extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaOrderCore.class);
	
	@Resource
	private SaScoreFlowCore saScoreFlowCore;
	
	@Resource
	private SaOrderController saOrderController;
	
	@Autowired
	private SysProxyUserServive sysProxyUserServive;
	
	@Autowired
	private SysUserServive sysUserServive;
	
	@Autowired
	private SaCardServive saCardService;
	
	
	@RequestMapping("save")
	@ResponseBody
	public String save(SaOrder sa,SaDealRecord saDealRecord){
		PageModel pageModel = new PageModel();
		String retStr = "";
		try {
			String checkStr = checkIsNull(sa);
			if(!checkStr.equals("notNull")){
				pageModel.setCode(2);
				pageModel.setMsg(checkStr+" is null");
				_log.info("{} {}",checkStr,"is null");
				return JSONObject.toJSONString(pageModel);
			}
			SaScoreFlow saScoreFlow;
			Long amount = sa.getScoreReturnAmount();
			if(sa.getProxyId().equals("0")){
				retStr = saOrderController.save(sa);
				saScoreFlow = getSaScoreFlowInstance(sa, saDealRecord);
				retStr = saScoreFlowCore.save(saScoreFlow);
			}else{
				sa.setScoreReturnAmount(Math.round(amount*0.8));
				retStr = saOrderController.save(sa);
				saScoreFlow = getSaScoreFlowInstance(sa, saDealRecord);
				retStr = saScoreFlowCore.save(saScoreFlow);
				
				sa.setScoreReturnAmount(Math.round(amount*0.2)); //代理用户积分返送总额 ****
				List<SysProxyUser> proxyUser = sysProxyUserServive.infoSysProxyUser(sa.getProxyId());
				sa.setUserId(proxyUser.get(0).getUserId());
				retStr = saOrderController.save(sa);
				saScoreFlow = getSaScoreFlowInstance(sa, saDealRecord);
				retStr = saScoreFlowCore.save(saScoreFlow);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("订单：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
			return JSONObject.toJSONString(pageModel);
		}
		return retStr;
	}
	
	@RequestMapping(value="delete")
	@ResponseBody
	public String delete(String dealOrderId){
		PageModel pageModel = new PageModel();
		String retStr = "";
		if(isNull(dealOrderId)){
			pageModel.setCode(2);
			pageModel.setMsg("dealOrderId is null");
			return JSONObject.toJSONString(pageModel);
		}
		retStr = saOrderController.delete(dealOrderId);
		return retStr;
	}
	
	@RequestMapping(value="update")
	@ResponseBody
	public String update(SaOrder saOrder){
		String retStr = saOrderController.update(saOrder);
		return retStr;
	}
	
	@RequestMapping(value="list")
	@ResponseBody
	public String select(String dealOrderId,Integer start,Integer end,String userId){
		String retStr = "";
		retStr = saOrderController.select(dealOrderId,start,end,userId);
		return retStr;
	}
	
	public SaScoreFlow getSaScoreFlowInstance(SaOrder saOrder,SaDealRecord saDealRecord) throws Exception{
		SaScoreFlow saScoreFlow = new SaScoreFlow(); //积分流水
		saScoreFlow.setDealRecordNumber(saOrder.getDealRecordNumber()); //交易记录号
		saScoreFlow.setMerchantOrderNumber(saOrder.getMerchantOrderNumber()); //商户订单号
		saScoreFlow.setTripTime(saDealRecord.getTripTime()); //出行时间
		saScoreFlow.setScoreFlowDirection(saOrder.getDealType()); //积分流向
		saScoreFlow.setCreateTime(saDealRecord.getDealEndTime()); //创建时间
		saScoreFlow.setComment(saDealRecord.getDealComment()); //交易记录备注
		saScoreFlow.setFlowType(saOrder.getDealType()); //流水类型****
		saScoreFlow.setCurrentDealScore(saOrder.getScoreReturnAmount());
		
		SaCard saCard = new SaCard();
		saCard.setUserId(saOrder.getUserId());
		saCard.setFlag(saOrder.getDealType());
		saCard.setRemainPart(saOrder.getScoreReturnAmount());
		saCardService.updateAll(saCard);
		List<SaCard> listCard = saCardService.infoSaCard(saOrder.getUserId());
		
		saScoreFlow.setRemainScore(listCard.get(0).getRemainPart()); //剩余积分****
		saScoreFlow.setProductType(saOrder.getProductType());
		SysUser user = sysUserServive.info(saOrder.getUserId());
		//saScoreFlow.setOneselfAccount(null); //本方账号
		saScoreFlow.setOthersAccount(user.getLoginAccount()); //对方账号
		//saScoreFlow.setOneselfCardNumber(null); //本方卡号
		saScoreFlow.setOthersCardNumber(listCard.get(0).getCardNumber()); //对方卡号
		//saScoreFlow.setBillNumber(null); //账单号
		saScoreFlow.setUserId(saOrder.getUserId()); //用户Id
		return saScoreFlow;
	}
		
	
	
}
