package org.xxpay.boot.card.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.dal.dao.card.model.SaDealRecord;

public interface SaDealRecordServive {
	
	int insertSaDealRecord(SaDealRecord saDealRecord) throws Exception;
	
	int deleteSaDealRecord(@Param("dealRecordId")String dealRecordId) throws Exception;
	
	int updateSaDealRecord(SaDealRecord saDealRecord) throws Exception;
	
	PageBean selectSaDealRecord(Integer start,Integer end,SaDealRecord saDealRecord) throws Exception;
	
	List<SaDealRecord> info(@Param("dealRecordId")String dealRecordId) throws Exception;
	
	List<Map<String, Integer>> totalDeal(SaDealRecord saDealRecord) throws Exception;
}
