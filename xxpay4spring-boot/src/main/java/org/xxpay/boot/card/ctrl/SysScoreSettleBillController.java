package org.xxpay.boot.card.ctrl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.SaCardServive;
import org.xxpay.boot.card.service.SaScoreFlowServive;
import org.xxpay.boot.card.service.SysScoreSettleBillService;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;
import org.xxpay.dal.dao.card.model.SysUser;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

@Component
public class SysScoreSettleBillController extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SysScoreSettleBillController.class);
	
	@Autowired
	private SysScoreSettleBillService sysScoreSettleBillService;
	
	@Autowired 
	private SaScoreFlowServive saScoreFlowServive;
	
	@Autowired
	private SaCardServive saCardServive;
	
	@Autowired
	private SysUserServive sysUserServive;
	
	public String save(SysScoreSettleBill scoreSettleBill){
		PageModel pageModel = new PageModel();
		try {
			List<String> userIdList = saScoreFlowServive.countUserId();
			long sum;
			for(String id:userIdList){
				sum = saScoreFlowServive.noConfigList(id);
				List<SaCard> saCard = saCardServive.infoSaCard(id);
				SysUser sysUser = sysUserServive.info(id);
				scoreSettleBill.setBillNumber(randomBill());
				scoreSettleBill.setUserId(id);
				scoreSettleBill.setLoginAccount(sysUser.getLoginAccount());
				scoreSettleBill.setCardNumber(saCard.get(0).getCardNumber());
				scoreSettleBill.setBillDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
				scoreSettleBill.setCurrentSettleScore(sum);
				scoreSettleBill.setBillStatus('0');
				sysScoreSettleBillService.insertSettleBill(scoreSettleBill);
			}
			_log.info("积分结算账单结算成功,保存记录数：{}",userIdList.size());
			pageModel.setCode(0);
			pageModel.setMsg("success");
			return JSONObject.toJSONString(pageModel);
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分结算账单：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String delete(String billId){
		PageModel pageModel = new PageModel();
		try {
			int result = sysScoreSettleBillService.deleteSettleBill(billId);
			if(result > 0){
				_log.info("积分结算账单删除成功,删除记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分结算账单：{}","删除失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String update(SysScoreSettleBill scoreSettleBill){
		PageModel pageModel = new PageModel();
		try {
			int result = sysScoreSettleBillService.updateSettleBill(scoreSettleBill);
			if(result > 0) _log.info("积分结算账单修改成功,修改记录数：{}",result);
			pageModel.setCode(0);
			pageModel.setMsg("success");
			return JSONObject.toJSONString(pageModel);
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分结算账单：{}","修改失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}

	public String select(Integer start,Integer end,SysScoreSettleBill scoreSettleBill){
		PageModel pageModel = new PageModel();
		List<SysScoreSettleBill> list;
		PageBean pageBean = new PageBean();
		try {
			String billId = scoreSettleBill.getBillId();
			if(billId != null && !billId.equals(" ")){
				list = sysScoreSettleBillService.infoSettleBill(billId);
				pageBean.setList(list);
				pageBean.setTotal(list.size());
			}else{
				pageBean = sysScoreSettleBillService.selectSettleBill(start,end,scoreSettleBill);
			}
			if(pageBean.getTotal() > 0){
				_log.info("积分结算账单查询成功,查询记录数：{}",pageBean.getTotal());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(pageBean.getList());
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分结算账单：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String limit(String userId){
		PageModel pageModel = new PageModel();
		List<SysScoreSettleBill> list;
		try {
			list = sysScoreSettleBillService.limitSettleBill(userId);
			if(list.size() > 0){
				_log.info("积分结算账单查询成功,查询记录数：{}",list.size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(list);
				pageModel.setCount(list.size());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分结算账单：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(SysScoreSettleBill scoreSettleBill){
		PageModel pageModel = new PageModel();
		List list;
		try {
			list = sysScoreSettleBillService.totalBill(scoreSettleBill);
			if(list.size() > 0){
				_log.info("查询成功,查询记录数：{}",list.size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(list);
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分流水：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String excel(SysScoreSettleBill scoreSettleBill,Integer start,Integer end){
		PageModel pageModel = new PageModel();
		PageBean pageBean = new PageBean();
		try {
			pageBean = sysScoreSettleBillService.selectSettleBill(start,end,scoreSettleBill);
			if(pageBean.getTotal() > 0){
				importExcel(pageBean.getList(),"积分结算账单表");
				_log.info("积分结算账单查询成功,：{}",pageBean.getList().size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分结算账单：{}","导出失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
}
