package org.xxpay.boot.card.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SaSettleConfigServive;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SaSettleConfig;

@Service
public class SaSettleConfigServiveImpl extends CardBaseService implements SaSettleConfigServive {

	@Override
	public List<SaSettleConfig> selectSaSettleConfig(String userId)
			throws Exception {
		return super.selectSaSettleConfig(userId);
	}
	
}
