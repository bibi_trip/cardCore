package org.xxpay.boot.card.common;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int start;
	
	private int end;
	
	private int total;
	
	private List<T> list;

	public Page() {
		super();
	}

	public Page(int start, int end, int total, List<T> list) {
		super();
		this.start = start;
		this.end = end;
		this.total = total;
		this.list = list;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}
	
}
