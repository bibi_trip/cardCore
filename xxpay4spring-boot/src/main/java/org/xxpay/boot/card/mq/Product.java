package org.xxpay.boot.card.mq;

import javax.jms.Destination;
import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class Product {
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public void send(String message){
		Destination destination = new ActiveMQQueue("card.queue");
		jmsTemplate.convertAndSend(destination, message);
	}
	
	@JmsListener(destination = "card.queue")
	public void receiveQueue(String text){
		System.out.println("接收信息报文====>>>"+text);
	}
}
