package org.xxpay.boot.card.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;

public interface SysScoreSettleBillService {
	
	int insertSettleBill(SysScoreSettleBill scoreSettleBill) throws Exception;
	
	int deleteSettleBill(String billId) throws Exception;
	
	int updateSettleBill(SysScoreSettleBill scoreSettleBill) throws Exception;
	
	List<SysScoreSettleBill> infoSettleBill(String billId) throws Exception;
	
	PageBean selectSettleBill(int start,int end,SysScoreSettleBill scoreSettleBill) throws Exception;
	
	List<SysScoreSettleBill> limitSettleBill(String userId) throws Exception;
	
	List<Map<String, Integer>> totalBill(SysScoreSettleBill scoreSettleBill) throws Exception;
}
