package org.xxpay.boot.card.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SysScoreSettleBillService;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;

@Service
public class SysScoreSettleBillServiceImpl extends CardBaseService implements SysScoreSettleBillService{

	@Override
	public int insertSettleBill(SysScoreSettleBill scoreSettleBill) throws Exception{
		return super.insertSettleBill(scoreSettleBill);
	}

	@Override
	public int deleteSettleBill(String billId) throws Exception {
		return super.deleteSettleBill(billId);
	}

	@Override
	public int updateSettleBill(SysScoreSettleBill scoreSettleBill)
			throws Exception {
		return super.updateSettleBill(scoreSettleBill);
	}

	@Override
	public List<SysScoreSettleBill> infoSettleBill(String billId)
			throws Exception {
		return super.infoSettleBill(billId);
	}

	@Override
	public PageBean selectSettleBill(int start,int end,SysScoreSettleBill scoreSettleBill) throws Exception {
		return super.selectSettleBill(start,end,scoreSettleBill);
	}

	@Override
	public List<SysScoreSettleBill> limitSettleBill(String userId) throws Exception {
		return super.limitSettleBill(userId);
	}

	@Override
	public List<Map<String, Integer>> totalBill(
			SysScoreSettleBill scoreSettleBill) throws Exception {
		return super.totalBill(scoreSettleBill);
	}
	
}
