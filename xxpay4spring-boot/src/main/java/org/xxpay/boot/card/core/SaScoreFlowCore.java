package org.xxpay.boot.card.core;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.ctrl.SaScoreFlowController;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.druid.sql.visitor.functions.Char;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 积分流水管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/saScoreFlow")
public class SaScoreFlowCore extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaScoreFlowCore.class);
	
	@Resource
	private SaScoreFlowController saScoreFlowController;
	
	@RequestMapping("save")
	@ResponseBody
	public String save(SaScoreFlow saScoreFlow){
		PageModel pageModel = new PageModel();
		String retStr = "";
		try{
			String checkStr = checkIsNull(saScoreFlow);
			if(checkStr.equals("notNull")){
				pageModel.setCode(2);
				pageModel.setMsg(checkStr+" is null");
				_log.info("{} {}",checkStr,"is null");
				return JSONObject.toJSONString(pageModel);
			}
			retStr = saScoreFlowController.save(saScoreFlow);
		}catch(Exception e){
			e.printStackTrace();
			_log.info("积分流水：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
			return JSONObject.toJSONString(pageModel);
		}finally{
		}
		return retStr;
	}
	
	@RequestMapping(value="delete")
	@ResponseBody
	public String delete(String scoreFlowId){
		PageModel pageModel = new PageModel();
		String retStr = "";
		if(isNull(scoreFlowId)){
			pageModel.setCode(2);
			pageModel.setMsg("scoreFlowId is null");
			return JSONObject.toJSONString(pageModel);
		}
		retStr = saScoreFlowController.delete(scoreFlowId);
		return retStr;
	}
	
	@RequestMapping(value="update")
	@ResponseBody
	public String update(SaScoreFlow saScoreFlow){
		String retStr = saScoreFlowController.update(saScoreFlow);
		return retStr;
	}
	
	@RequestMapping(value="list")
	@ResponseBody
	public String select(SaScoreFlow saScoreFlow,Integer start,Integer end){
		String retStr = "";
		retStr = saScoreFlowController.select(saScoreFlow,start,end);
		return retStr;
	}
	
	@RequestMapping(value="noScore")
	@ResponseBody
	public String select(String userId){
		String retStr = "";
		retStr = saScoreFlowController.select(userId);
		return retStr;
	}
	
	@RequestMapping(value="count")
	@ResponseBody
	public String select(SaScoreFlow saScoreFlow){
		String retStr = "";
		retStr = saScoreFlowController.select(saScoreFlow);
		return retStr;
	}
	
	@RequestMapping(value="countUserId")
	@ResponseBody
	public String countUserId(){
		String retStr = "";
		retStr = saScoreFlowController.countUserId();
		return retStr;
	}
	
	@RequestMapping(value="excel")
	@ResponseBody
	public String excel(SaScoreFlow saScoreFlow,Integer start,Integer end){
		String retStr = "";
		retStr = saScoreFlowController.excel(saScoreFlow, start, end);
		return retStr;
	}
}
