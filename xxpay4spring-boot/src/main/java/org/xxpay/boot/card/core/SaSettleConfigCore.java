package org.xxpay.boot.card.core;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.ctrl.SaCardController;
import org.xxpay.boot.card.ctrl.SaSettleConfigController;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 卡内余额管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/saSettleConfig")
public class SaSettleConfigCore extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaSettleConfigCore.class);
	
	@Resource
	private SaSettleConfigController saSettleConfigController;
	
	@RequestMapping(value="list")
	@ResponseBody
	public String select(String userId){
		String retStr = "";
		retStr = saSettleConfigController.list(userId);
		return retStr;
	}
	
}
