package org.xxpay.boot.card.core;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.ctrl.SysScoreSettleBillController;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 账单管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/saScoreSettleBill")
public class SaScoreSettleBillCore extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaScoreSettleBillCore.class);
	
	@Resource
	private SysScoreSettleBillController scoreSettleBillController;
	
	@RequestMapping("save")
	@ResponseBody
	public String save(SysScoreSettleBill scoreSettleBill){
		PageModel pageModel = new PageModel();
		String retStr = "";
		String checkStr = checkIsNull(scoreSettleBill);
		if(checkStr.equals("notNull")){
			pageModel.setCode(2);
			pageModel.setMsg(checkStr+" is null");
			_log.info("{} {}",checkStr,"is null");
			return JSONObject.toJSONString(pageModel);
		}
		retStr = scoreSettleBillController.save(scoreSettleBill);
		return retStr;
	}
	
	@RequestMapping(value="delete")
	@ResponseBody
	public String delete(String billId){
		PageModel pageModel = new PageModel();
		String retStr = "";
		if(isNull(billId)){
			pageModel.setCode(2);
			pageModel.setMsg("billId is null");
			return JSONObject.toJSONString(pageModel);
		}
		retStr = scoreSettleBillController.delete(billId);
		return retStr;
	}
	
	@RequestMapping(value="update")
	@ResponseBody
	public String update(SysScoreSettleBill scoreSettleBill){
		String retStr = scoreSettleBillController.update(scoreSettleBill);
		return retStr;
	}
	
	@RequestMapping(value="list")
	@ResponseBody
	public String select(Integer start,Integer end,SysScoreSettleBill scoreSettleBill){
		String retStr = "";
		retStr = scoreSettleBillController.select(start,end,scoreSettleBill);
		return retStr;
	}
	
	@RequestMapping(value="limitList")
	@ResponseBody
	public String select(String userId){
		String retStr = "";
		retStr = scoreSettleBillController.limit(userId);
		return retStr;
	}
	
	@RequestMapping(value="count")
	@ResponseBody
	public String select(SysScoreSettleBill scoreSettleBill){
		String retStr = "";
		retStr = scoreSettleBillController.select(scoreSettleBill);
		return retStr;
	}
	
	@RequestMapping(value="excel")
	@ResponseBody
	public String excel(Integer start,Integer end,SysScoreSettleBill scoreSettleBill){
		String retStr = "";
		retStr = scoreSettleBillController.excel(scoreSettleBill,start,end);
		return retStr;
	}
}
