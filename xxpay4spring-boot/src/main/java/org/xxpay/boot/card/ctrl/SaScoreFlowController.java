package org.xxpay.boot.card.ctrl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SaOrderServive;
import org.xxpay.boot.card.service.SaScoreFlowServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

@Component
public class SaScoreFlowController extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaScoreFlowController.class);
	
	@Autowired
	private SaScoreFlowServive saScoreFlowServive;
	
	public String save(SaScoreFlow saScoreFlow){
		PageModel pageModel = new PageModel();
		try {
			String flowNumber = "";
			if(saScoreFlow.getScoreFlowDirection() == '1'){
				flowNumber = "R"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+(new Random().nextInt(900)+100);
			}else{
				flowNumber = "C"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+(new Random().nextInt(900)+100);
			}
			saScoreFlow.setScoreFlowNumber(flowNumber); //积分流水号
			int result = saScoreFlowServive.insertSaScoreFlow(saScoreFlow);
			if(result > 0){
				_log.info("积分流水添加成功,保存记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分流水：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String delete(String scoreFlowId){
		PageModel pageModel = new PageModel();
		try {
			int result = saScoreFlowServive.deleteSaScoreFlow(scoreFlowId);
			if(result > 0){
				_log.info("积分流水删除成功,删除记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分流水：{}","删除失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String update(SaScoreFlow saScoreFlow){
		PageModel pageModel = new PageModel();
		try {
			int result = saScoreFlowServive.updateSaScoreFlow(saScoreFlow);
			if(result > 0) _log.info("积分流水修改成功,修改记录数：{}",result);
			pageModel.setCode(0);
			pageModel.setMsg("success");
			return JSONObject.toJSONString(pageModel);
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分流水：{}","修改失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(SaScoreFlow saScoreFlow,Integer start,Integer end){
		PageModel pageModel = new PageModel();
		List<SaScoreFlow> list;
		PageBean pageBean = new PageBean();
		try {
			String scoreFlowId = saScoreFlow.getScoreFlowId();
			if(scoreFlowId != null && !scoreFlowId.equals(" ")){
				list = saScoreFlowServive.infoSaScoreFlow(scoreFlowId);
				pageBean.setList(list);
				pageBean.setTotal(list.size());
			}else{
				pageBean = saScoreFlowServive.selectSaScoreFlow(saScoreFlow,start,end);
			}
			if(pageBean.getTotal() > 0){
				_log.info("积分流水查询成功,查询记录数：{}",pageBean.getTotal());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(pageBean.getList());
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分流水：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(SaScoreFlow saScoreFlow){
		PageModel pageModel = new PageModel();
		List list;
		try {
			list = saScoreFlowServive.totalScore(saScoreFlow);
			if(list.size() > 0){
				_log.info("查询成功,查询记录数：{}",list.size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(list);
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分流水：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(String userId){
		PageModel pageModel = new PageModel();
		Integer score = 0;
		try {
			score = saScoreFlowServive.noConfigScore(userId);
			if(score > 0){
				_log.info("未出账积分查询成功,：{}",score);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setCount(score);
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("未出账积分：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String countUserId(){
		PageModel pageModel = new PageModel();
		try {
			List<String> userIdList = saScoreFlowServive.countUserId();
			System.out.println(userIdList);
			if(userIdList.size() > 0){
				_log.info("未出账用户查询成功,：{}",userIdList.size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setCount(userIdList.size());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("未出账用户：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String excel(SaScoreFlow saScoreFlow,Integer start,Integer end){
		PageModel pageModel = new PageModel();
		PageBean pageBean = new PageBean();
		try {
			pageBean = saScoreFlowServive.selectSaScoreFlow(saScoreFlow,start,end);
			if(pageBean.getTotal() > 0){
				importExcel(pageBean.getList(),"积分流水表");
				_log.info("积分流水查询成功,：{}",pageBean.getList().size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("未出账积分：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
}
