package org.xxpay.boot.card.core;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.ctrl.SaDealRecordController;
import org.xxpay.boot.card.ctrl.SaOrderController;
import org.xxpay.boot.card.ctrl.SaScoreFlowController;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SysProxyUser;
import org.xxpay.dal.dao.card.model.SysUser;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

@Controller
@RequestMapping("/api/togetherCore")
public class UnifyCore extends CardBaseService{
	
	@Resource
	private SaDealRecordController saDealRecordController;
	
	@Resource
	private SaOrderController saOrderController;
	
	@Resource
	private SaScoreFlowController saScoreFlowController;
	
	@RequestMapping(value="save")
	@ResponseBody
	public String save(SaDealRecord saDealRecord){
		String retStr = "";
		try {
			List<SysUser> user = super.infoSysUser(saDealRecord.getLoginAccount());
			saDealRecord.setUserId(user.get(0).getUserId());
			retStr = saDealRecordController.save(saDealRecord); //生成交易记录
			
			Long amount = saDealRecord.getScoreReturnAmount();
			SaOrder saOrder = new SaOrder(); //订单
			saOrder.setDealRecordNumber(saDealRecord.getDealRecordNumber()); //交易记录号
			saOrder.setMerchantOrderNumber(saDealRecord.getMerchantOrderNumber()); //商户订单号
			saOrder.setDealType(saDealRecord.getDealType()); //交易类型
			saOrder.setProductType(saDealRecord.getProductType()); //商品类型
			saOrder.setCalculateTime(null); //清算时间
			saOrder.setOrderAmount(saDealRecord.getOrderAmount()); //订单总额
			saOrder.setDealBringScoreType(saDealRecord.getDealBringScoreType()); //创建时生产积分类型
			saOrder.setDealBringScoreRate(saDealRecord.getDealBringScoreRate()); //创建时生产积分比率
			saOrder.setScoreReturnAmount(Math.round(amount*0.8)); //积分返送总额 ****
			saOrder.setDealComment(saDealRecord.getDealComment()); //交易备注
			saOrder.setDealStatus(saDealRecord.getDealStatus()); //交易状态
			saOrder.setDealCreateTime(saDealRecord.getDealCreateTime()); //交易创建时间
			saOrder.setDealEndTime(null); //交易完成时间****
			saOrder.setUserId(saDealRecord.getUserId()); //用户ID
			retStr = saOrderController.save(saOrder); //生成订单
			retStr = saveScoreFlow(saOrder, saDealRecord);
			//如果代理号不为空将为代理用户生成订单
			if(saDealRecord.getProxyNumber() != null && !saDealRecord.getProxyNumber().equals(" ")){
				saOrder.setProxyId(saDealRecord.getProxyNumber()); //代理号
				saOrder.setScoreReturnAmount(Math.round(amount*0.2)); //代理用户积分返送总额 ****
				//通过代理号获取代理用户ID
				List<SysProxyUser> sysProxyUser = super.infoSysProxyUser(saDealRecord.getProxyNumber()); 
				saOrder.setUserId(sysProxyUser.get(0).getUserId());
				retStr = saOrderController.save(saOrder); //代理用户生成订单
				retStr = saveScoreFlow(saOrder, saDealRecord);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retStr;
	}
	
	public String saveScoreFlow(SaOrder saOrder,SaDealRecord saDealRecord) throws Exception{
		SaScoreFlow saScoreFlow = new SaScoreFlow(); //积分流水
		saScoreFlow.setDealRecordNumber(saOrder.getDealRecordNumber()); //交易记录号
		saScoreFlow.setMerchantOrderNumber(saOrder.getMerchantOrderNumber()); //商户订单号
		saScoreFlow.setTripTime(saDealRecord.getTripTime()); //出行时间
		saScoreFlow.setScoreFlowDirection(saOrder.getDealType()); //积分流向
		saScoreFlow.setCreateTime(saDealRecord.getDealEndTime()); //创建时间
		saScoreFlow.setComment(saDealRecord.getDealComment()); //交易记录备注
		saScoreFlow.setFlowType('0'); //流水类型****
		saScoreFlow.setRemainScore(1000L); //剩余积分****
		
		SysUser user = super.infoSysUserByUserId(saOrder.getUserId());
		List<SaCard> saCard = super.infoSaCard(saOrder.getUserId());
		saScoreFlow.setOneselfAccount(null); //本方账号
		saScoreFlow.setOthersAccount(user.getLoginAccount()); //对方账号
		saScoreFlow.setOneselfCardNumber(null); //本方卡号
		saScoreFlow.setOthersCardNumber(saCard.get(0).getCardNumber()); //对方卡号
		saScoreFlow.setBillNumber(null); //账单号
		saScoreFlow.setUserId(saOrder.getUserId()); //用户Id
		String ret = saScoreFlowController.save(saScoreFlow);
		return ret;
	}

}
