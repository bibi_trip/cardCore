package org.xxpay.boot.card.core;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.ctrl.SaCardController;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 卡内余额管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/saCard")
public class SaCardCore extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaCardCore.class);
	
	@Resource
	private SaCardController saCardController;
	
	@RequestMapping("save")
	@ResponseBody
	public String save(SaCard saCard){
		saCard.setCardNumber(randomCard());
		PageModel pageModel = new PageModel();
		String retStr = "";
		String checkStr = checkIsNull(saCard);
		if(!checkStr.equals("notNull")){
			pageModel.setCode(2);
			pageModel.setMsg(checkStr+" is null");
			_log.info("{} {}",checkStr,"is null");
			return JSONObject.toJSONString(pageModel);
		} 	
		retStr = saCardController.save(saCard);
		return retStr;
	}
	
	@RequestMapping(value="update")
	@ResponseBody
	public String update(SaCard saCard){
		String retStr = saCardController.update(saCard);
		return retStr;
	}
	
	@RequestMapping(value="list")
	@ResponseBody
	public String select(String userId,Integer start,Integer end){
		String retStr = "";
		retStr = saCardController.list(userId,start,end);
		return retStr;
	}
	
}
