package org.xxpay.boot.card.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SaOrderServive;
import org.xxpay.boot.card.service.SaScoreFlowServive;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;

@Service
public class SaScoreFlowServiveImpl extends CardBaseService implements SaScoreFlowServive{

	@Override
	public int insertSaScoreFlow(SaScoreFlow saScoreFlow) throws Exception {
		int result = super.addSaScoreFlow(saScoreFlow);
		return result;
	}

	@Override
	public int deleteSaScoreFlow(String scoreFlowId) throws Exception {
		return super.deleteSaScoreFlow(scoreFlowId);
	}

	@Override
	public int updateSaScoreFlow(SaScoreFlow saScoreFlow) throws Exception {
		return super.updateSaScoreFlow(saScoreFlow);
	}

	@Override
	public PageBean selectSaScoreFlow(SaScoreFlow saScoreFlow,Integer start,Integer end) throws Exception {
		return super.selectSaScoreFlow(saScoreFlow,start,end);
	}

	@Override
	public List<SaScoreFlow> infoSaScoreFlow(String scoreFlowId) throws Exception {
		return super.infoSaScoreFlow(scoreFlowId);
	}

	@Override
	public int noConfigScore(String userId) throws Exception {
		return super.noConfigScore(userId);
	}

	public List<Map<String, Integer>> totalScore(SaScoreFlow saScoreFlow) throws Exception {
		return super.totalScore(saScoreFlow);
	}

	@Override
	public List<String> countUserId() throws Exception {
		return super.countUserId();
	}

	@Override
	public long noConfigList(String userId) throws Exception {
		return super.noConfigList(userId);
	}

}
