package org.xxpay.boot.card.ctrl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.mq.Product;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SysUser;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

@Component
public class SaDealRecordController extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaDealRecordController.class);
	
	@Autowired
	private SaDealRecordServive saDealRecordServive;
	
	@Resource
	private SaOrderController saOrderController;
	
	@Autowired
	private SysUserServive sysUserService;
	
	@Resource
	private Product product;

	public String save(SaDealRecord saDealRecord){
		PageModel pageModel = new PageModel();
		try {
			int result = saDealRecordServive.insertSaDealRecord(saDealRecord);
			if(result > 0){
				_log.info("交易记录添加成功,保存记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String delete(String dealRecordId){
		PageModel pageModel = new PageModel();
		try {
			int result = saDealRecordServive.deleteSaDealRecord(dealRecordId);
			if(result > 0){
				_log.info("交易记录删除成功,删除记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录：{}","删除失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String update(SaDealRecord saDealRecord){
		PageModel pageModel = new PageModel();
		try {
			int result = saDealRecordServive.updateSaDealRecord(saDealRecord);
			if(result > 0){
				_log.info("交易记录修改成功,修改记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录：{}","修改失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(String dealRecordId,Integer start,Integer end,SaDealRecord saDealRecord){
		PageModel pageModel = new PageModel();
		List<SaDealRecord> list;
		PageBean pageBean = new PageBean();
		try {
			if(dealRecordId != null && !dealRecordId.equals(" ")){
				list = (List<SaDealRecord>) saDealRecordServive.info(dealRecordId);
				pageBean.setList(list);
				pageBean.setTotal(list.size());
			}else{
				
				if(saDealRecord.getProductType() == '8'){ //全部为8 因此不进行商品类型条件查询
					saDealRecord.setProductType(null);
				}
				if(saDealRecord.getDealType() == '8'){ //全部为8 因此不进行交易类型条件查询
					saDealRecord.setDealType(null);
				}
				pageBean = saDealRecordServive.selectSaDealRecord(start,end,saDealRecord);
				product.send(pageBean.getList().get(0).toString());
			}
			if(pageBean.getTotal() > 0){
				_log.info("交易记录查询成功,查询记录数：{}",pageBean.getTotal());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(pageBean.getList());
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(SaDealRecord saDealRecord){
		PageModel pageModel = new PageModel();
		List list;
		try {
			list = saDealRecordServive.totalDeal(saDealRecord);
			if(list.size() > 0){
				_log.info("交易记录总数查询成功,查询记录数：{}",list.size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(list);
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录总数：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String excel(SaDealRecord saDealRecord,Integer start,Integer end){
		PageModel pageModel = new PageModel();
		PageBean pageBean = new PageBean();
		try {
			if(saDealRecord.getProductType() == '8'){ //全部为8 因此不进行商品类型条件查询
				saDealRecord.setProductType(null);
			}
			if(saDealRecord.getDealType() == '8'){ //全部为8 因此不进行交易类型条件查询
				saDealRecord.setDealType(null);
			}
			pageBean = saDealRecordServive.selectSaDealRecord(start, end, saDealRecord);
			if(pageBean.getTotal() > 0){
				importExcel(pageBean.getList(),"交易记录表");
				_log.info("交易记录查询成功,：{}",pageBean.getList().size());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
}
