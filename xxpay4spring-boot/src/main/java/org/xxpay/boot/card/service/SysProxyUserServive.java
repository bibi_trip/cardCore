package org.xxpay.boot.card.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysProxyUser;
import org.xxpay.dal.dao.card.model.SysUser;

public interface SysProxyUserServive {
	
	List<SysProxyUser> infoSysProxyUser(String proxyNumber) throws Exception;
}
