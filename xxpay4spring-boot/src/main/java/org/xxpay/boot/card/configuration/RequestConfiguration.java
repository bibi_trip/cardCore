package org.xxpay.boot.card.configuration;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.xxpay.boot.card.interceptor.RequestInterceptor;

@Configuration
public class RequestConfiguration extends WebMvcConfigurerAdapter{
	
	@Resource
	private RequestInterceptor requestInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requestInterceptor).addPathPatterns("/api/saCard/save");
		super.addInterceptors(registry);
	}

}
