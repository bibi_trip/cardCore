package org.xxpay.boot.card.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysUser;

import com.alibaba.fastjson.JSONObject;

public interface SaCardServive {
	
	int insertSaCard(SaCard saCard)throws Exception ;
	
	List<SaCard> infoSaCard(String userId) throws Exception;
	
	int updateSaCard(String cardNumber,String flag,String remainPart) throws Exception;
	
	int updateAll(SaCard saCard) throws Exception;
	
	PageBean listSaCard(int start,int end) throws Exception;
}
