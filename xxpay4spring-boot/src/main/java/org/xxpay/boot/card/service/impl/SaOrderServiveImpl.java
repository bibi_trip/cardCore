package org.xxpay.boot.card.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SaOrderServive;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;

@Service
public class SaOrderServiveImpl extends CardBaseService implements SaOrderServive{

	@Override
	public int insertSaOrder(SaOrder dealOrder) throws Exception {
		int result = super.addSaOrder(dealOrder);
		return result;
	}

	@Override
	public int deleteSaOrder(String dealOrderId) throws Exception {
		int result = super.deleteSaOrder(dealOrderId);
		return result;
	}

	@Override
	public int updateSaOrder(SaOrder dealOrder) throws Exception {
		return super.updateSaOrder(dealOrder);
	}

	@Override
	public PageBean selectSaOrder(int start,int end,String userId) throws Exception {
		return super.selectSaOrder(start,end,userId);
	}

	@Override
	public List<SaOrder> info(String dealOrderId) throws Exception {
		return super.infoSaOrder(dealOrderId);
	}


}
