package org.xxpay.boot.card.ctrl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.SaCardServive;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SaOrderServive;
import org.xxpay.boot.card.service.SaSettleConfigServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SaSettleConfig;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

@Component
public class SaSettleConfigController extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaSettleConfigController.class);
	
	@Autowired
	private SaSettleConfigServive saSettleConfigServive;

	public String list(String userId){
		PageModel pageModel = new PageModel();
		try {
			List<SaSettleConfig> list = null;
			if(userId != null && !userId.equals(" ")){
				list = saSettleConfigServive.selectSaSettleConfig(userId);
			}
			if(list.size() > 0){
				_log.info("出账配置查询成功,查询记录数：{}",list.size());
				pageModel.setCode(1);
				pageModel.setMsg(getDate(list));
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("出账配置：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
}
