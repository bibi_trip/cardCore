package org.xxpay.boot.card.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;

public interface SaScoreFlowServive {
	
	int insertSaScoreFlow(SaScoreFlow saScoreFlow) throws Exception;
	
	int deleteSaScoreFlow(String scoreFlowId) throws Exception;
	
	int updateSaScoreFlow(SaScoreFlow saScoreFlow) throws Exception;
	
	PageBean selectSaScoreFlow(SaScoreFlow saScoreFlow,Integer start,Integer end) throws Exception;
	
	List<SaScoreFlow> infoSaScoreFlow(String scoreFlowId) throws Exception;
	
	int noConfigScore(String userId) throws Exception;
	
	List<Map<String, Integer>> totalScore(SaScoreFlow saScoreFlow) throws Exception;
	
	List<String> countUserId() throws Exception;
	
	long noConfigList(String userId) throws Exception;
	
}
