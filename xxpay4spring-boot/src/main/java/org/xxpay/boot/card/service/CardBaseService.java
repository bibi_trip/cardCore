package org.xxpay.boot.card.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.common.Page;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.ctrl.PayOrderController;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.mapper.SaCardMapper;
import org.xxpay.dal.dao.card.mapper.SaDealRecordMapper;
import org.xxpay.dal.dao.card.mapper.SaOrderMapper;
import org.xxpay.dal.dao.card.mapper.SaScoreFlowMapper;
import org.xxpay.dal.dao.card.mapper.SaSettleConfigMapper;
import org.xxpay.dal.dao.card.mapper.SysProxyUserMapper;
import org.xxpay.dal.dao.card.mapper.SysScoreSettleBillMapper;
import org.xxpay.dal.dao.card.mapper.SysUserMapper;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SaSettleConfig;
import org.xxpay.dal.dao.card.model.SysProxyUser;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;
import org.xxpay.dal.dao.card.model.SysUser;

import com.fasterxml.jackson.databind.annotation.JsonAppend.Attr;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class CardBaseService<T> extends CommonUtils<T>{
	
	private final MyLog _log = MyLog.getLog(CardBaseService.class);
	
	@Autowired
	private SaDealRecordMapper saDealRecordMapper;
	
	@Autowired 
	private SaOrderMapper saOrderMapper;
	
	@Autowired
	private SaScoreFlowMapper saScoreFlowMapper;
	
	@Autowired
	private SysUserMapper sysUserMapper;
	
	@Autowired
	private SaCardMapper saCardMapper;
	
	@Autowired
	private SysScoreSettleBillMapper sysScoreSettleBillMapper;
	
	@Autowired
	private SysProxyUserMapper sysProxyUserMapper;
	
	@Autowired
	private SaSettleConfigMapper saSettleConfigMapper;
	
	/*交易记录---开始*/
	public int addSaDealRecord(SaDealRecord saDealRecord)throws Exception{
		int result = saDealRecordMapper.insertSaDealRecord(saDealRecord);
		return result;
	}
	
	public int deleteSaDealRecord(String dealRecordId)throws Exception{
		int result = saDealRecordMapper.delete(dealRecordId);
		return result;
	}
	
	public int updateSaDealRecord(SaDealRecord saDealRecord)throws Exception{
		int result = saDealRecordMapper.update(saDealRecord);
		return result;
	}
	
	public PageBean selectSaDealRecord(Integer start,Integer end,SaDealRecord saDealRecord)throws Exception{
		PageHelper.offsetPage(start,end,false);
		List<SaDealRecord> list = saDealRecordMapper.select(saDealRecord);
		int total = saDealRecordMapper.count(saDealRecord);
		return getInstance(start, end, total, list);
	}
	
	public List<SaDealRecord> infoSaDealRecord(String dealRecordId)throws Exception{
		List<SaDealRecord> list = saDealRecordMapper.info(dealRecordId);
		return list;
	}
	
	public List<Map<String, Integer>> totalDeal(SaDealRecord saDealRecord) throws Exception {
		saDealRecord.setDealType('0');
		int expend = saDealRecordMapper.totalDeal(saDealRecord);
		saDealRecord.setDealType('1');
		int income = saDealRecordMapper.totalDeal(saDealRecord);
		saDealRecord.setDealType(null);
		int count = saDealRecordMapper.count(saDealRecord);
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("count", count);
		map.put("expend", expend);
		map.put("income", income);
		List<Map<String, Integer>> list = new ArrayList<Map<String,Integer>>();
		list.add(map);
		return list;
	}
	/*交易记录---结束*/
	
	/*订单---开始*/
	public int addSaOrder(SaOrder dealOrder)throws Exception{
		int result = saOrderMapper.insertSaOrder(dealOrder);
		return result;
	}
	
	public int deleteSaOrder(String dealOrderId)throws Exception{
		int result = saOrderMapper.deleteSaOrder(dealOrderId);
		return result;
	}
	
	public int updateSaOrder(SaOrder dealOrder) throws Exception {
		int result = saOrderMapper.updateSaOrder(dealOrder);
		return result;
	}
	
	public PageBean selectSaOrder(int start,int end,String userId)throws Exception{
		PageHelper.offsetPage(start,end,false);
		List<SaOrder> list = saOrderMapper.selectSaOrder(userId);
		return getInstance(start, end, list.size(), list);
	}
	
	public List<SaOrder> infoSaOrder(String dealOrderId)throws Exception{
		return saOrderMapper.infoSaOrder(dealOrderId);
	}
	/*订单---结束*/
	
	/*积分流水---开始*/
	public int addSaScoreFlow(SaScoreFlow saScoreFlow)throws Exception{
		int result = saScoreFlowMapper.insertSaScoreFlow(saScoreFlow);
		return result;
	}
	
	public int deleteSaScoreFlow(String scoreFlowId) throws Exception{
		return saScoreFlowMapper.deleteSaScoreFlow(scoreFlowId);
	}
	
	public int updateSaScoreFlow(SaScoreFlow saScoreFlow) throws Exception {
		return saScoreFlowMapper.updateSaScoreFlow(saScoreFlow);
	}
	
	public PageBean selectSaScoreFlow(SaScoreFlow saScoreFlow,Integer start,Integer end) throws Exception {
		PageHelper.offsetPage(start,end,false);
		List<SaScoreFlow> list = saScoreFlowMapper.selectSaScoreFlow(saScoreFlow);
		int total = saScoreFlowMapper.count(saScoreFlow);
		return getInstance(start, end, total, list);
	}
	
	public List<SaScoreFlow> infoSaScoreFlow(String scoreFlowId) throws Exception {
		return saScoreFlowMapper.infoSaScoreFlow(scoreFlowId);
	}
	
	public int noConfigScore(String userId) throws Exception {
		return saScoreFlowMapper.noConfigScore(userId);
	}
	
	public List<Map<String, Integer>> totalScore(SaScoreFlow saScoreFlow) throws Exception {
		saScoreFlow.setScoreFlowDirection('0');
		int expend = saScoreFlowMapper.totalScore(saScoreFlow);
		saScoreFlow.setScoreFlowDirection('1');
		int income = saScoreFlowMapper.totalScore(saScoreFlow);
		saScoreFlow.setScoreFlowDirection(null);
		int count = saScoreFlowMapper.count(saScoreFlow);
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("count", count);
		map.put("expend", expend);
		map.put("income", income);
		List<Map<String, Integer>> list = new ArrayList<Map<String,Integer>>();
		list.add(map);
		return list;
		
	}

	public List<String> countUserId() throws Exception {
		return saScoreFlowMapper.countUserId();
	}
	
	public long noConfigList(String userId) throws Exception {
		return saScoreFlowMapper.noConfigList(userId);
	}
	/*积分流水---结束*/
	
	/*用户---开始*/
	public List<SysUser> infoSysUser(String loginAccount)throws Exception{
		return sysUserMapper.infoSysUser(loginAccount);
	}
	public SysUser infoSysUserByUserId(String userId) throws Exception {
		return sysUserMapper.info(userId);
	}
	/*用户---结束*/
	
	/*代理用户---开始*/
	public List<SysProxyUser> infoSysProxyUser(String proxyNumber)throws Exception {
		return sysProxyUserMapper.infoSysProxyUser(proxyNumber);
	}
	/*代理用户---结束*/
	
	/*积分卡---开始*/
	public List<SaCard> infoSaCard(String userId)throws Exception{
		return saCardMapper.infoSaCard(userId);
	}
	
	public int updateSaCard(String cardNumber,String flag,String remainPart)throws Exception{
		return saCardMapper.updateSaCard(cardNumber,flag,remainPart);
	}
	
	public int updateAll(SaCard saCard)throws Exception{
		return saCardMapper.updateAll(saCard);
	}
	
	public int insertSaCard(SaCard saCard) throws Exception{
		return saCardMapper.insertSaCard(saCard);
	}
	
	public PageBean listSaCard(int start,int end)throws Exception{
		PageHelper.offsetPage(start,end,false);
		List<SaCard> list = saCardMapper.listSaCard();
		return getInstance(start, end, list.size(), list);
	}
	/*积分卡---结束*/
	
	/*积分结算账单---开始*/
	public int insertSettleBill(SysScoreSettleBill scoreSettleBill) throws Exception{
		return sysScoreSettleBillMapper.insertSettleBill(scoreSettleBill);
	}
	
	public int deleteSettleBill(String billId) throws Exception {
		return sysScoreSettleBillMapper.deleteSettleBill(billId);
	}
	
	public int updateSettleBill(SysScoreSettleBill scoreSettleBill)
			throws Exception {
		return sysScoreSettleBillMapper.updateSettleBill(scoreSettleBill);
	}

	public List<SysScoreSettleBill> infoSettleBill(String billId)
			throws Exception {
		return sysScoreSettleBillMapper.infoSettleBill(billId);
	}

	public PageBean selectSettleBill(int start,int end,SysScoreSettleBill scoreSettleBill) throws Exception {
		PageHelper.offsetPage(start,end,false);
		List<SysScoreSettleBill> list = sysScoreSettleBillMapper.selectSettleBill(scoreSettleBill);
		int total = sysScoreSettleBillMapper.count(scoreSettleBill);
		return getInstance(start, end, total, list);
	}
	
	public List<SysScoreSettleBill> limitSettleBill(String userId) throws Exception {
		return sysScoreSettleBillMapper.limitSettleBill(userId);
	}
	
	public List<Map<String, Integer>> totalBill(
			SysScoreSettleBill scoreSettleBill) throws Exception {
		scoreSettleBill.setBillStatus('0');
		int expend = sysScoreSettleBillMapper.totalBill(scoreSettleBill);
		scoreSettleBill.setBillStatus('1');
		int income = sysScoreSettleBillMapper.totalBill(scoreSettleBill);
		scoreSettleBill.setBillStatus(null);
		int count = sysScoreSettleBillMapper.count(scoreSettleBill);
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("count", count);
		map.put("expend", expend);
		map.put("income", income);
		List<Map<String, Integer>> list = new ArrayList<Map<String,Integer>>();
		list.add(map);
		return list;
	}
	/*积分结算账单---结束*/
	
	/*出账配置表---开始*/
	public List<SaSettleConfig> selectSaSettleConfig(String userId)
			throws Exception {
		return saSettleConfigMapper.selectSaSettleConfig(userId);
	}
	/*出账配置表---结束*/
}
