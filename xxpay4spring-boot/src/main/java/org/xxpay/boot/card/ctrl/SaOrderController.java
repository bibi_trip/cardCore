package org.xxpay.boot.card.ctrl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.SaCardServive;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SaOrderServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

@Component
public class SaOrderController {
	
	private final static MyLog _log = MyLog.getLog(SaOrderController.class);
	
	@Autowired
	private SaOrderServive saOrderService;
	
	@Autowired
	private SaCardServive saCardServive;
	
	@Resource
	private SaScoreFlowController saScoreFlowController;
	
	public String save(SaOrder dealOrder){
		PageModel pageModel = new PageModel();
		try {
			int result = saOrderService.insertSaOrder(dealOrder);
			if(result > 0) _log.info("{}订单添加成功,保存记录数：{}","用户",result);
			pageModel.setCode(0);
			pageModel.setMsg("success");
			return JSONObject.toJSONString(pageModel);
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("订单：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String delete(String dealOrderId){
		PageModel pageModel = new PageModel();
		try {
			int result = saOrderService.deleteSaOrder(dealOrderId);
			if(result > 0){
				_log.info("订单删除成功,删除记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("订单：{}","删除失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String update(SaOrder dealOrder){
		PageModel pageModel = new PageModel();
		try {
			int result = saOrderService.updateSaOrder(dealOrder);
			if(result > 0) _log.info("{}订单修改成功,修改记录数：{}","用户",result);
			pageModel.setCode(0);
			pageModel.setMsg("success");
			return JSONObject.toJSONString(pageModel);
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("订单：{}","修改失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String select(String dealOrderId,Integer start,Integer end,String userId){
		PageModel pageModel = new PageModel();
		List<SaOrder> list;
		PageBean pageBean = new PageBean();
		try {
			if(dealOrderId != null && !dealOrderId.equals(" ")){
				list = saOrderService.info(dealOrderId);
				pageBean.setList(list);
				pageBean.setTotal(list.size());
			}else{
				pageBean = saOrderService.selectSaOrder(start,end,userId);
			}
			if(pageBean.getTotal() > 0){
				_log.info("订单查询成功,查询记录数：{}",pageBean.getTotal());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(pageBean.getList());
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("订单：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
}
