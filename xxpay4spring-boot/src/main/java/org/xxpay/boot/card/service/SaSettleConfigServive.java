package org.xxpay.boot.card.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.card.model.SaSettleConfig;

public interface SaSettleConfigServive {
	
	List<SaSettleConfig> selectSaSettleConfig(String userId) throws Exception;
}
