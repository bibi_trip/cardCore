package org.xxpay.boot.card.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.dal.dao.card.model.SaDealRecord;

@Service
public class SaDealRecordServiveImpl extends CardBaseService implements SaDealRecordServive{

	@Override
	public int insertSaDealRecord(SaDealRecord saDealRecord) throws Exception {
		int result = super.addSaDealRecord(saDealRecord);
		return result;
	}

	@Override
	public int deleteSaDealRecord(String dealRecordId) throws Exception {
		int result = super.deleteSaDealRecord(dealRecordId);
		return result;
	}
	
	@Override
	public int updateSaDealRecord(SaDealRecord saDealRecord) throws Exception {
		int result = super.updateSaDealRecord(saDealRecord);
		return result;
	}
	
	@Override
	public PageBean selectSaDealRecord(Integer start,Integer end,SaDealRecord saDealRecord) throws Exception {
		return super.selectSaDealRecord(start,end,saDealRecord);
	}
	
	public List<SaDealRecord> info(String dealRecordId) throws Exception {
		return super.infoSaDealRecord(dealRecordId);
	}

	@Override
	public List<Map<String, Integer>> totalDeal(SaDealRecord saDealRecord) throws Exception {
		return super.totalDeal(saDealRecord);
	}

}
