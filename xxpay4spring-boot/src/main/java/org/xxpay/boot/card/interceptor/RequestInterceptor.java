package org.xxpay.boot.card.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.xxpay.boot.card.ctrl.SaCardController;
import org.xxpay.common.util.MyLog;

@Component
public class RequestInterceptor implements HandlerInterceptor{
	
	private final static MyLog _log = MyLog.getLog(RequestInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		_log.info("拦截器启动{}", "......");
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		_log.info("postHandle{}", "......");
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		_log.info("afterCompletion{}", "......");
		
	}

}
