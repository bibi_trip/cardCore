package org.xxpay.boot.card.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.dal.dao.card.model.SysUser;

@Service
public class SysUserServiceImpl extends CardBaseService implements SysUserServive{
	
	public List<SysUser> infoSysUser(String loginAccount) throws Exception{
		return super.infoSysUser(loginAccount);
	}

	@Override
	public SysUser info(String userId) throws Exception {
		return super.infoSysUserByUserId(userId);
	}
}
