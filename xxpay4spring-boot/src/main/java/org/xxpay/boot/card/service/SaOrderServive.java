package org.xxpay.boot.card.service;

import java.util.List;

import org.xxpay.boot.card.common.PageBean;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;

public interface SaOrderServive {
	
	int insertSaOrder(SaOrder dealOrder) throws Exception;
	
	int deleteSaOrder(String dealOrderId) throws Exception;
	
	int updateSaOrder(SaOrder dealOrder) throws Exception;
	
	PageBean selectSaOrder(int start,int end,String userId) throws Exception;
	
	List<SaOrder> info(String dealOrderId) throws Exception;
}
