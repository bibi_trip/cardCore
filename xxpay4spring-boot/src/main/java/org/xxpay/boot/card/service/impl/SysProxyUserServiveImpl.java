package org.xxpay.boot.card.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.xxpay.boot.card.service.CardBaseService;
import org.xxpay.boot.card.service.SysProxyUserServive;
import org.xxpay.dal.dao.card.model.SysProxyUser;

@Service
public class SysProxyUserServiveImpl extends CardBaseService implements SysProxyUserServive{

	@Override
	public List<SysProxyUser> infoSysProxyUser(String proxyNumber)
			throws Exception {
		return super.infoSysProxyUser(proxyNumber);
	}

}
