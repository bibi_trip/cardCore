package org.xxpay.boot.card.core;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.ctrl.SaDealRecordController;
import org.xxpay.boot.card.service.SysUserServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysUser;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 交易记录管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/saDealRecord")
public class SaDealRecordCore extends CommonUtils{
	
	private final static MyLog _log = MyLog.getLog(SaDealRecordCore.class);
	
	@Resource
	private SaDealRecordController saDealRecordController;
	
	@Resource
	private SaOrderCore saOrderCore;
	
	@Autowired
	private SysUserServive sysUserServive;
	
	@RequestMapping(value="save",method=RequestMethod.POST)
	@ResponseBody
	public String save(SaDealRecord saDealRecord,HttpSession session,HttpServletResponse response){
		PageModel pageModel = new PageModel();
		String retStr = "";
		JSONObject json;
		try {
			saDealRecord = getSaDealRecordInstance(saDealRecord);
			String checkStr = checkIsNull(saDealRecord);
			if(checkStr.equals("notNull")){
				pageModel.setCode(2);
				pageModel.setMsg(checkStr+" is null");
				_log.info("{} {}",checkStr,"is null");
				return JSONObject.toJSONString(pageModel);
			}
			List<SysUser> sysUser = sysUserServive.infoSysUser(saDealRecord.getLoginAccount());
			saDealRecord.setUserId(sysUser.get(0).getUserId());
			retStr = saDealRecordController.save(saDealRecord);
			json = JSONObject.parseObject(retStr);
			if(json.getString("code").equals("1")){
				return retStr;
			}
			SaOrder saOrder = getSaOrderInstance(saDealRecord);
			retStr = saOrderCore.save(saOrder, saDealRecord);
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("交易记录：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
			return JSONObject.toJSONString(pageModel);
		}
		return retStr;
	}
	
	@RequestMapping(value="delete")
	@ResponseBody
	public String delete(String dealRecordId){
		PageModel pageModel = new PageModel();
		String retStr = "";
		if(isNull(dealRecordId)){
			pageModel.setCode(2);
			pageModel.setMsg("dealRecordId is null");
			return JSONObject.toJSONString(pageModel);
		}
		retStr = saDealRecordController.delete(dealRecordId);
		return retStr;
	}
	
	@RequestMapping(value="update")
	@ResponseBody
	public String update(SaDealRecord saDealRecord){
		String retStr = saDealRecordController.update(saDealRecord);
		return retStr;
	}
	
	@RequestMapping(value="list")
	@ResponseBody
	public String select(String dealRecordId,Integer start,Integer end,SaDealRecord saDealRecord){
		String retStr = "";
		retStr = saDealRecordController.select(dealRecordId,start,end,saDealRecord);
		return retStr;
	}
	
	@RequestMapping(value="count")
	@ResponseBody
	public String select(SaDealRecord saDealRecord){
		String retStr = "";
		retStr = saDealRecordController.select(saDealRecord);
		return retStr;
	}
	
	@RequestMapping(value="excel")
	@ResponseBody
	public String excel(SaDealRecord saDealRecord,Integer start,Integer end){
		String retStr = "";
		retStr = saDealRecordController.excel(saDealRecord,start,end);
		return retStr;
	}
	
	public SaDealRecord getSaDealRecordInstance(SaDealRecord saDealRecord){
		String userRegion = "用户区域";
		String proxyNumber = "dl123456";
		String dealDetail = "交易详情";
		char dealStatus = '0';
		char dealBringScoreType = '1'; /*创建时生产积分类型。值：根据利润0|根据交易额1*/
		double dealBringScoreRate = 9.00; /*创建时生产积分比率*/
		double dealProxyAccountRate = 3.00; /*创建时代理分账比率*/
		String dealComment = "";
		char dealType = saDealRecord.getDealType();
		char productType = saDealRecord.getProductType();
		if(productType == '0'){
			dealComment += "其他";
		}else if(productType == '1'){
			dealComment += "火车票";
		}else if(productType == '2'){
			dealComment += "酒店";
		}else if(productType == '3'){
			dealComment += "旅游";
		}else if(productType == '4'){
			dealComment += "门票";
		}else if(productType == '5'){
			dealComment += "机票";
		}else if(productType == '6'){
			dealComment += "机场服务";
		}else if(productType == '7'){
			dealComment += "签证";
		}
		if(dealType == '0'){
			dealComment += "退款";
		}else if(dealType == '1'){
			dealComment = "买入"+dealComment;
		}
		
		saDealRecord.setUserRegion(userRegion);
		saDealRecord.setProxyNumber(proxyNumber);
		saDealRecord.setDealDetail(dealDetail);
		saDealRecord.setDealComment(dealComment);
		saDealRecord.setDealStatus(dealStatus);
		saDealRecord.setDealRecordNumber(randomDeal());
		saDealRecord.setScoreReturnAmount(Long.valueOf(new DecimalFormat("######0").format(saDealRecord.getOrderAmount())));
		saDealRecord.setDealBringScoreType(dealBringScoreType);
		saDealRecord.setDealBringScoreRate(dealBringScoreRate);
		saDealRecord.setDealProxyAccountRate(dealProxyAccountRate);
		return saDealRecord;
	}
	
	public SaOrder getSaOrderInstance(SaDealRecord saDealRecord){
		SaOrder saOrder = new SaOrder(); //订单
		saOrder.setDealRecordNumber(saDealRecord.getDealRecordNumber()); //交易记录号
		saOrder.setMerchantOrderNumber(saDealRecord.getMerchantOrderNumber()); //商户订单号
		saOrder.setProxyId(saDealRecord.getProxyNumber()); //代理号
		saOrder.setDealType(saDealRecord.getDealType()); //交易类型
		saOrder.setProductType(saDealRecord.getProductType()); //商品类型
		saOrder.setCalculateTime(null); //清算时间
		saOrder.setOrderAmount(saDealRecord.getOrderAmount()); //订单总额
		saOrder.setDealBringScoreType(saDealRecord.getDealBringScoreType()); //创建时生产积分类型
		saOrder.setDealBringScoreRate(saDealRecord.getDealBringScoreRate()); //创建时生产积分比率
		saOrder.setScoreReturnAmount(saDealRecord.getScoreReturnAmount()); //积分返送总额
		saOrder.setDealComment(saDealRecord.getDealComment()); //交易备注
		saOrder.setDealStatus(saDealRecord.getDealStatus()); //交易状态
		saOrder.setDealCreateTime(saDealRecord.getDealCreateTime()); //交易创建时间
		//saOrder.setDealEndTime(null); //交易完成时间****
		saOrder.setUserId(saDealRecord.getUserId()); //用户ID
		return saOrder;
	}
	
	
}
