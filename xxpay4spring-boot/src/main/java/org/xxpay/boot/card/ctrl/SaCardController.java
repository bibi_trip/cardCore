package org.xxpay.boot.card.ctrl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xxpay.boot.card.common.CommonUtils;
import org.xxpay.boot.card.common.PageBean;
import org.xxpay.boot.card.service.SaCardServive;
import org.xxpay.boot.card.service.SaDealRecordServive;
import org.xxpay.boot.card.service.SaOrderServive;
import org.xxpay.common.util.MyLog;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaDealRecord;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;
import org.xxpay.dal.dao.plugin.PageModel;

import com.alibaba.fastjson.JSONObject;

@Component
public class SaCardController{
	
	private final static MyLog _log = MyLog.getLog(SaCardController.class);
	
	@Autowired
	private SaCardServive saCardServive;
	
	public String save(SaCard saCard){
		PageModel pageModel = new PageModel();
		try {
			int result = saCardServive.insertSaCard(saCard);
			if(result > 0){
				_log.info("积分卡添加成功,保存记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分卡：{}","添加失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String update(SaCard saCard){
		PageModel pageModel = new PageModel();
		try {
			int result = saCardServive.updateAll(saCard);
			if(result > 0){
				_log.info("积分卡修改成功,修改记录数：{}",result);
				pageModel.setCode(0);
				pageModel.setMsg("success");
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分卡：{}","修改失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
	public String list(String userId,Integer start,Integer end){
		PageModel pageModel = new PageModel();
		try {
			List<SaCard> list;
			PageBean pageBean = new PageBean();
			if(userId != null && !userId.equals(" ")){
				list = saCardServive.infoSaCard(userId);
				pageBean.setList(list);
				pageBean.setTotal(list.size());
			}else{
				pageBean = saCardServive.listSaCard(start,end);
			}
			if(pageBean.getTotal() > 0){
				_log.info("积分卡查询成功,查询记录数：{}",pageBean.getTotal());
				pageModel.setCode(0);
				pageModel.setMsg("success");
				pageModel.setList(pageBean.getList());
				pageModel.setCount(pageBean.getTotal());
				return JSONObject.toJSONString(pageModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
			_log.info("积分卡：{}","查询失败");
			pageModel.setCode(1);
			pageModel.setMsg("fail");
		}
		return JSONObject.toJSONString(pageModel);
	}
	
}
