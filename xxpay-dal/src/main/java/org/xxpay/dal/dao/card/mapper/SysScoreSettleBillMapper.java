package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysScoreSettleBill;

public interface SysScoreSettleBillMapper {
	
	int insertSettleBill(SysScoreSettleBill scoreSettleBill);
	
	int deleteSettleBill(@Param("billId")String billId);
	
	int updateSettleBill(SysScoreSettleBill scoreSettleBill);
	
	List<SysScoreSettleBill> infoSettleBill(@Param("billId")String billId);
	
	List<SysScoreSettleBill> selectSettleBill(SysScoreSettleBill scoreSettleBill);
	
	List<SysScoreSettleBill> limitSettleBill(@Param("userId")String userId);
	
	int count(SysScoreSettleBill scoreSettleBill);
	
	int totalBill(SysScoreSettleBill scoreSettleBill);
}
