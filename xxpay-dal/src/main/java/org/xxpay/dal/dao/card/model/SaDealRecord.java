package org.xxpay.dal.dao.card.model;

import org.xxpay.dal.dao.card.FieldAnnotation;

/**
 * 
 * 交易记录实体类
 * @author Administrator
 *
 */
public class SaDealRecord {
	
	/**
	 * 交易记录ID
	 * 
	 */
	@FieldAnnotation(name="交易记录ID")
	private String dealRecordId;
	
	/**
	 * 交易记录号
	 * 
	 */
	@FieldAnnotation(name="交易记录号")
	private String dealRecordNumber;
	
	/**
	 * 商户订单号
	 * 
	 */
	@FieldAnnotation(name="商户订单号")
	private String merchantOrderNumber;
	
	/**
	 * 用户登录账号
	 * 
	 */
	@FieldAnnotation(name="用户登录账号")
	private String loginAccount;
	
	/**
	 * 用户区域
	 * 
	 */
	@FieldAnnotation(name="用户区域")
	private String userRegion;
	
	/**
	 * 代理号
	 * 
	 */
	@FieldAnnotation(name="代理号")
	private String proxyNumber;
	
	/**
	 * 交易类型
	 * 
	 */
	@FieldAnnotation(name="交易类型")
	private Character dealType;
	
	/**
	 * 商品类型
	 * 
	 */
	@FieldAnnotation(name="商品类型")
	private Character productType;
	
	/**
	 * 出行时间
	 * 
	 */
	@FieldAnnotation(name="出行时间")
	private String tripTime;
	
	/**
	 * 订单总额
	 * 
	 */
	@FieldAnnotation(name="订单总额")
	private Double orderAmount;
	
	/**
	 * 创建时生产积分类型
	 * 
	 */
	@FieldAnnotation(name="创建时生产积分类型")
	private Character dealBringScoreType;

	/**
	 * 创建时生产积分比率
	 * 
	 */
	@FieldAnnotation(name="创建时生产积分比率")
	private Double dealBringScoreRate;
	
	/**
	 * 创建时代理分账比率
	 * 
	 */
	@FieldAnnotation(name="创建时代理分账比率")
	private Double dealProxyAccountRate;
	
	/**
	 * 积分返还总额
	 * 
	 */
	@FieldAnnotation(name="积分返还总额")
	private Long scoreReturnAmount;
	
	/**
	 * 订单详情
	 * 
	 */
	@FieldAnnotation(name="订单详情")
	private String orderDetail;
	
	/**
	 * 交易详情
	 * 
	 */
	@FieldAnnotation(name="交易详情")
	private String dealDetail;
	
	/**
	 * 交易备注
	 * 
	 */
	@FieldAnnotation(name="交易备注")
	private String dealComment;
	
	/**
	 * 交易状态
	 * 
	 */
	@FieldAnnotation(name="交易状态")
	private Character dealStatus;
	
	/**
	 * 交易创建时间
	 * 
	 */
	@FieldAnnotation(name="交易创建时间")
	private String dealCreateTime;
	
	/**
	 * 交易完成时间
	 * 
	 */
	@FieldAnnotation(name="交易完成时间")
	private String dealEndTime;
	
	/**
	 * 外键，用户ID
	 * 
	 */
	@FieldAnnotation(name="用户ID")
	private String userId;
	
	/**
	 * 是否删除
	 * 
	 */
	private Character IsDelete;
	
	/**
	 * 标志
	 * 
	 */
	private Character flag;
	
	/**
	 * 状态
	 * 
	 */
	private Character status;
	
	/**
	 * 卡号
	 * 
	 */
	private String cardNumber;

	public String getCardNumber() {
		return cardNumber;
	}

	public SaDealRecord(String dealRecordId, String dealRecordNumber,
			String merchantOrderNumber, String loginAccount, String userRegion,
			String proxyNumber, Character dealType, Character productType,
			String tripTime, Double orderAmount, Character dealBringScoreType,
			Double dealBringScoreRate, Double dealProxyAccountRate,
			Long scoreReturnAmount, String orderDetail, String dealDetail,
			String dealComment, Character dealStatus, String dealCreateTime,
			String dealEndTime, String userId, Character isDelete, Character flag,
			Character status, String cardNumber) {
		super();
		this.dealRecordId = dealRecordId;
		this.dealRecordNumber = dealRecordNumber;
		this.merchantOrderNumber = merchantOrderNumber;
		this.loginAccount = loginAccount;
		this.userRegion = userRegion;
		this.proxyNumber = proxyNumber;
		this.dealType = dealType;
		this.productType = productType;
		this.tripTime = tripTime;
		this.orderAmount = orderAmount;
		this.dealBringScoreType = dealBringScoreType;
		this.dealBringScoreRate = dealBringScoreRate;
		this.dealProxyAccountRate = dealProxyAccountRate;
		this.scoreReturnAmount = scoreReturnAmount;
		this.orderDetail = orderDetail;
		this.dealDetail = dealDetail;
		this.dealComment = dealComment;
		this.dealStatus = dealStatus;
		this.dealCreateTime = dealCreateTime;
		this.dealEndTime = dealEndTime;
		this.userId = userId;
		IsDelete = isDelete;
		this.flag = flag;
		this.status = status;
		this.cardNumber = cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public SaDealRecord() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDealRecordId() {
		return dealRecordId;
	}

	public void setDealRecordId(String dealRecordId) {
		this.dealRecordId = dealRecordId;
	}

	public String getDealRecordNumber() {
		return dealRecordNumber;
	}

	public void setDealRecordNumber(String dealRecordNumber) {
		this.dealRecordNumber = dealRecordNumber;
	}

	public String getMerchantOrderNumber() {
		return merchantOrderNumber;
	}

	public void setMerchantOrderNumber(String merchantOrderNumber) {
		this.merchantOrderNumber = merchantOrderNumber;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public String getUserRegion() {
		return userRegion;
	}

	public void setUserRegion(String userRegion) {
		this.userRegion = userRegion;
	}

	public String getProxyNumber() {
		return proxyNumber;
	}

	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}

	public Character getDealType() {
		return dealType;
	}

	public void setDealType(Character dealType) {
		this.dealType = dealType;
	}

	public Character getProductType() {
		return productType;
	}

	public void setProductType(Character productType) {
		this.productType = productType;
	}

	public String getTripTime() {
		return tripTime;
	}

	public void setTripTime(String tripTime) {
		this.tripTime = tripTime;
	}

	public Double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Character getDealBringScoreType() {
		return dealBringScoreType;
	}

	public void setDealBringScoreType(Character dealBringScoreType) {
		this.dealBringScoreType = dealBringScoreType;
	}

	public Double getDealBringScoreRate() {
		return dealBringScoreRate;
	}

	public void setDealBringScoreRate(Double dealBringScoreRate) {
		this.dealBringScoreRate = dealBringScoreRate;
	}

	public Double getDealProxyAccountRate() {
		return dealProxyAccountRate;
	}

	public void setDealProxyAccountRate(Double dealProxyAccountRate) {
		this.dealProxyAccountRate = dealProxyAccountRate;
	}

	public Long getScoreReturnAmount() {
		return scoreReturnAmount;
	}

	public void setScoreReturnAmount(Long scoreReturnAmount) {
		this.scoreReturnAmount = scoreReturnAmount;
	}

	public String getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(String orderDetail) {
		this.orderDetail = orderDetail;
	}

	public String getDealDetail() {
		return dealDetail;
	}

	public void setDealDetail(String dealDetail) {
		this.dealDetail = dealDetail;
	}

	public String getDealComment() {
		return dealComment;
	}

	public void setDealComment(String dealComment) {
		this.dealComment = dealComment;
	}

	public Character getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(Character dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getDealCreateTime() {
		return dealCreateTime;
	}

	public void setDealCreateTime(String dealCreateTime) {
		this.dealCreateTime = dealCreateTime;
	}

	public String getDealEndTime() {
		return dealEndTime;
	}

	public void setDealEndTime(String dealEndTime) {
		this.dealEndTime = dealEndTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Character isDelete) {
		IsDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SaDealRecord [dealRecordId=" + dealRecordId
				+ ", dealRecordNumber=" + dealRecordNumber
				+ ", merchantOrderNumber=" + merchantOrderNumber
				+ ", loginAccount=" + loginAccount + ", userRegion="
				+ userRegion + ", proxyNumber=" + proxyNumber + ", dealType="
				+ dealType + ", productType=" + productType + ", tripTime="
				+ tripTime + ", orderAmount=" + orderAmount
				+ ", dealBringScoreType=" + dealBringScoreType
				+ ", dealBringScoreRate=" + dealBringScoreRate
				+ ", dealProxyAccountRate=" + dealProxyAccountRate
				+ ", scoreReturnAmount=" + scoreReturnAmount + ", orderDetail="
				+ orderDetail + ", dealDetail=" + dealDetail + ", dealComment="
				+ dealComment + ", dealStatus=" + dealStatus
				+ ", dealCreateTime=" + dealCreateTime + ", dealEndTime="
				+ dealEndTime + ", userId=" + userId + ", IsDelete=" + IsDelete
				+ ", flag=" + flag + ", status=" + status + "]";
	}

	
}
