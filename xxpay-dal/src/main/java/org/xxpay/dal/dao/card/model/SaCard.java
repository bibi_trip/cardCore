package org.xxpay.dal.dao.card.model;

/**
 * 
 * 积分卡实体类
 * @author Administrator
 *
 */
public class SaCard {
		
	/**
	 * 主键，积分卡ID
	 * 
	 */
	private String cardId;
	
	/**
	 * 卡号
	 * 
	 */
	private String cardNumber;
	
	/**
	 * 出账配置号
	 * 
	 */
	private String configNumber;
	
	/**
	 * 用户登录账号
	 * 
	 */
	private String loginAccount;
	
	/**
	 * 剩余积分
	 * 
	 */
	private Long remainPart;
	
	/**
	 * 被冻结积分
	 * 
	 */
	private Long freezePart;
	
	/**
	 * 积分卡状态
	 * 
	 */
	private Character cardStatus;
	
	/**
	 * 创建时间
	 * 
	 */
	private String createTime;
	
	/**
	 * 外键，用户ID
	 * 
	 */
	private String userId;
	
	/**
	 * 是否删除
	 * 
	 */
	private Character isDelete;
	
	/**
	 * 标志
	 * 
	 */
	private Character flag;

	public SaCard() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SaCard(String cardId, String cardNumber, String configNumber,
			String loginAccount, Long remainPart, Long freezePart,
			Character cardStatus, String createTime, String userId, Character isDelete,
			Character flag) {
		super();
		this.cardId = cardId;
		this.cardNumber = cardNumber;
		this.configNumber = configNumber;
		this.loginAccount = loginAccount;
		this.remainPart = remainPart;
		this.freezePart = freezePart;
		this.cardStatus = cardStatus;
		this.createTime = createTime;
		this.userId = userId;
		this.isDelete = isDelete;
		this.flag = flag;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getConfigNumber() {
		return configNumber;
	}

	public void setConfigNumber(String configNumber) {
		this.configNumber = configNumber;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public Long getRemainPart() {
		return remainPart;
	}

	public void setRemainPart(Long remainPart) {
		this.remainPart = remainPart;
	}

	public Long getFreezePart() {
		return freezePart;
	}

	public void setFreezePart(Long freezePart) {
		this.freezePart = freezePart;
	}

	public Character getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(Character cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "SaCard [cardId=" + cardId + ", cardNumber=" + cardNumber
				+ ", configNumber=" + configNumber + ", loginAccount="
				+ loginAccount + ", remainPart=" + remainPart + ", freezePart="
				+ freezePart + ", cardStatus=" + cardStatus + ", createTime="
				+ createTime + ", userId=" + userId + ", isDelete=" + isDelete
				+ ", flag=" + flag + "]";
	}
	

}
