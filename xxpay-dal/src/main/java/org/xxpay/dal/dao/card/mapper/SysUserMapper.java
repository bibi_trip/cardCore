package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysUser;

public interface SysUserMapper {
	
	List<SysUser> infoSysUser(@Param("loginAccount")String loginAccount);
	
	SysUser info(@Param("userId")String userId);
}
