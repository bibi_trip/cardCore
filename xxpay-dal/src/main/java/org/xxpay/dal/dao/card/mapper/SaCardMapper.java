package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaCard;
import org.xxpay.dal.dao.card.model.SaOrder;

public interface SaCardMapper {
	
	int insertSaCard(SaCard saCard);
	
	List<SaCard> infoSaCard(@Param("userId")String userId);
	
	int updateSaCard(@Param("cardNumber")String cardNumber,@Param("flag")String flag,@Param("remainPart")String remainPart);
	
	int updateAll(SaCard saCard);
	
	List<SaCard> listSaCard();
}