package org.xxpay.dal.dao.card.model;

/**
 * 
 * 用户实体类
 * @author Administrator
 *
 */
public class SysUser {
	
	private String userId;
	
	private String loginAccount;
	
	private String mobliePhone;
	
	private String email;
	
	private String loginPwd;
	
	private String payPwd;
	
	private Character userType;
	
	private String tdOpenId;
	
	private String userRegion;
	
	private String registerTime;
	
	private String lastLoginTime;
	
	private String createTime;
	
	private String roleId;
	
	private String merchantCode;
	
	private int showCount;

	private Character is_delete;
	
	private Character flag;
	
	private Character status;

	public SysUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SysUser(String userId, String loginAccount, String mobliePhone,
			String email, String loginPwd, String payPwd, Character userType,
			String tdOpenId, String userRegion, String registerTime,
			String lastLoginTime, String createTime, String roleId,
			String merchantCode, int showCount, Character is_delete, Character flag,
			Character status) {
		super();
		this.userId = userId;
		this.loginAccount = loginAccount;
		this.mobliePhone = mobliePhone;
		this.email = email;
		this.loginPwd = loginPwd;
		this.payPwd = payPwd;
		this.userType = userType;
		this.tdOpenId = tdOpenId;
		this.userRegion = userRegion;
		this.registerTime = registerTime;
		this.lastLoginTime = lastLoginTime;
		this.createTime = createTime;
		this.roleId = roleId;
		this.merchantCode = merchantCode;
		this.showCount = showCount;
		this.is_delete = is_delete;
		this.flag = flag;
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public String getMobliePhone() {
		return mobliePhone;
	}

	public void setMobliePhone(String mobliePhone) {
		this.mobliePhone = mobliePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public String getPayPwd() {
		return payPwd;
	}

	public void setPayPwd(String payPwd) {
		this.payPwd = payPwd;
	}

	public Character getUserType() {
		return userType;
	}

	public void setUserType(Character userType) {
		this.userType = userType;
	}

	public String getTdOpenId() {
		return tdOpenId;
	}

	public void setTdOpenId(String tdOpenId) {
		this.tdOpenId = tdOpenId;
	}

	public String getUserRegion() {
		return userRegion;
	}

	public void setUserRegion(String userRegion) {
		this.userRegion = userRegion;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}

	public String getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public int getShowCount() {
		return showCount;
	}

	public void setShowCount(int showCount) {
		this.showCount = showCount;
	}

	public Character getIs_delete() {
		return is_delete;
	}

	public void setIs_delete(Character is_delete) {
		this.is_delete = is_delete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SysUser [userId=" + userId + ", loginAccount=" + loginAccount
				+ ", mobliePhone=" + mobliePhone + ", email=" + email
				+ ", loginPwd=" + loginPwd + ", payPwd=" + payPwd
				+ ", userType=" + userType + ", tdOpenId=" + tdOpenId
				+ ", userRegion=" + userRegion + ", registerTime="
				+ registerTime + ", lastLoginTime=" + lastLoginTime
				+ ", createTime=" + createTime + ", roleId=" + roleId
				+ ", merchantCode=" + merchantCode + ", showCount=" + showCount
				+ ", is_delete=" + is_delete + ", flag=" + flag + ", status="
				+ status + "]";
	}

	
}
