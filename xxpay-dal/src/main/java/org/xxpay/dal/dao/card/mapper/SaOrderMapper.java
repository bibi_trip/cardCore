package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaOrder;

public interface SaOrderMapper {
	
	int insertSaOrder(SaOrder dealOrderId);
	
	int deleteSaOrder(@Param("dealOrderId")String dealOrderId);
	
	int updateSaOrder(SaOrder saOrder);
	
	List<SaOrder> selectSaOrder(@Param("userId")String userId);
	
	List<SaOrder> infoSaOrder(@Param("dealOrderId")String dealOrderId);
}
