package org.xxpay.dal.dao.card.model;

/**
 * 
 * 订单实体类
 * @author Administrator
 *
 */
public class SaOrder {
	
	/**
	 * 订单ID
	 * 
	 */
	private String dealOrderId;
	
	/**
	 * 代理用户
	 * 
	 */
	private String proxyId;
	
	/**
	 * 交易记录号
	 * 
	 */
	private String dealRecordNumber;
	
	/**
	 * 商户订单号
	 * 
	 */
	private String merchantOrderNumber;

	/**
	 * 交易类型
	 * 
	 */
	private Character dealType;
	
	/**
	 * 商品类型
	 * 
	 */
	private Character productType;
	
	/**
	 * 清算时间
	 * 
	 */
	private String calculateTime;
	
	/**
	 * 订单总额
	 * 
	 */
	private Double orderAmount;
	
	/**
	 * 创建时生产积分类型
	 * 
	 */
	private Character dealBringScoreType;

	/**
	 * 创建时生产积分比率
	 * 
	 */
	private Double dealBringScoreRate;
	
	/**
	 * 积分返还总额
	 * 
	 */
	private Long scoreReturnAmount;
	
	/**
	 * 交易备注
	 * 
	 */
	private String dealComment;
	
	/**
	 * 交易状态
	 * 
	 */
	private Character dealStatus;
	
	/**
	 * 交易创建时间
	 * 
	 */
	private String dealCreateTime;
	
	/**
	 * 交易完成时间
	 * 
	 */
	private String dealEndTime;
	
	/**
	 * 外键，用户ID
	 * 
	 */
	private String userId;
	
	/**
	 * 是否删除
	 * 
	 */
	private Character IsDelete;
	
	/**
	 * 标志
	 * 
	 */
	private Character flag;
	
	/**
	 * 状态
	 * 
	 */
	private Character status;
	
	/**
	 * 卡号
	 * 
	 */
	private Character cardNumber;

	public Character getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(Character cardNumber) {
		this.cardNumber = cardNumber;
	}

	public SaOrder(String dealOrderId, String proxyId, String dealRecordNumber,
			String merchantOrderNumber, Character dealType, Character productType,
			String calculateTime, Double orderAmount, Character dealBringScoreType,
			Double dealBringScoreRate, Long scoreReturnAmount,
			String dealComment, Character dealStatus, String dealCreateTime,
			String dealEndTime, String userId, Character isDelete, Character flag,
			Character status, Character cardNumber) {
		super();
		this.dealOrderId = dealOrderId;
		this.proxyId = proxyId;
		this.dealRecordNumber = dealRecordNumber;
		this.merchantOrderNumber = merchantOrderNumber;
		this.dealType = dealType;
		this.productType = productType;
		this.calculateTime = calculateTime;
		this.orderAmount = orderAmount;
		this.dealBringScoreType = dealBringScoreType;
		this.dealBringScoreRate = dealBringScoreRate;
		this.scoreReturnAmount = scoreReturnAmount;
		this.dealComment = dealComment;
		this.dealStatus = dealStatus;
		this.dealCreateTime = dealCreateTime;
		this.dealEndTime = dealEndTime;
		this.userId = userId;
		IsDelete = isDelete;
		this.flag = flag;
		this.status = status;
		this.cardNumber = cardNumber;
	}

	public SaOrder() {
		super();
	}

	public String getDealOrderId() {
		return dealOrderId;
	}

	public void setDealOrderId(String dealOrderId) {
		this.dealOrderId = dealOrderId;
	}

	public String getProxyId() {
		return proxyId;
	}

	public void setProxyId(String proxyId) {
		this.proxyId = proxyId;
	}

	public String getDealRecordNumber() {
		return dealRecordNumber;
	}

	public void setDealRecordNumber(String dealRecordNumber) {
		this.dealRecordNumber = dealRecordNumber;
	}

	public String getMerchantOrderNumber() {
		return merchantOrderNumber;
	}

	public void setMerchantOrderNumber(String merchantOrderNumber) {
		this.merchantOrderNumber = merchantOrderNumber;
	}

	public Character getDealType() {
		return dealType;
	}

	public void setDealType(Character dealType) {
		this.dealType = dealType;
	}

	public Character getProductType() {
		return productType;
	}

	public void setProductType(Character productType) {
		this.productType = productType;
	}

	public String getCalculateTime() {
		return calculateTime;
	}

	public void setCalculateTime(String calculateTime) {
		this.calculateTime = calculateTime;
	}

	public Double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Double orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Character getDealBringScoreType() {
		return dealBringScoreType;
	}

	public void setDealBringScoreType(Character dealBringScoreType) {
		this.dealBringScoreType = dealBringScoreType;
	}

	public Double getDealBringScoreRate() {
		return dealBringScoreRate;
	}

	public void setDealBringScoreRate(Double dealBringScoreRate) {
		this.dealBringScoreRate = dealBringScoreRate;
	}

	public Long getScoreReturnAmount() {
		return scoreReturnAmount;
	}

	public void setScoreReturnAmount(Long scoreReturnAmount) {
		this.scoreReturnAmount = scoreReturnAmount;
	}

	public String getDealComment() {
		return dealComment;
	}

	public void setDealComment(String dealComment) {
		this.dealComment = dealComment;
	}

	public Character getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(Character dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getDealCreateTime() {
		return dealCreateTime;
	}

	public void setDealCreateTime(String dealCreateTime) {
		this.dealCreateTime = dealCreateTime;
	}

	public String getDealEndTime() {
		return dealEndTime;
	}

	public void setDealEndTime(String dealEndTime) {
		this.dealEndTime = dealEndTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Character isDelete) {
		IsDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SaOrder [dealOrderId=" + dealOrderId + ", proxyId=" + proxyId
				+ ", dealRecordNumber=" + dealRecordNumber
				+ ", merchantOrderNumber=" + merchantOrderNumber
				+ ", dealType=" + dealType + ", productType=" + productType
				+ ", calculateTime=" + calculateTime + ", orderAmount="
				+ orderAmount + ", dealBringScoreType=" + dealBringScoreType
				+ ", dealBringScoreRate=" + dealBringScoreRate
				+ ", scoreReturnAmount=" + scoreReturnAmount + ", dealComment="
				+ dealComment + ", dealStatus=" + dealStatus
				+ ", dealCreateTime=" + dealCreateTime + ", dealEndTime="
				+ dealEndTime + ", userId=" + userId + ", IsDelete=" + IsDelete
				+ ", flag=" + flag + ", status=" + status + "]";
	}
	
	
	
}
