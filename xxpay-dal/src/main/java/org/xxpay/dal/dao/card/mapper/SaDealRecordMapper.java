package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaDealRecord;


public interface SaDealRecordMapper{
	
	int insertSaDealRecord(SaDealRecord saDealRecord);
	
	int delete(@Param("dealRecordId")String dealRecordId);
	
	int update(SaDealRecord saDealRecord);
	
	List<SaDealRecord> select(SaDealRecord saDealRecord);
	
	List<SaDealRecord> info(@Param("dealRecordId")String dealRecordId);
	
	int count(SaDealRecord saDealRecord);
	
	int totalDeal(SaDealRecord saDealRecord);
	
}
