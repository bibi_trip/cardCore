package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SysProxyUser;
import org.xxpay.dal.dao.card.model.SysUser;

public interface SysProxyUserMapper {
	
	List<SysProxyUser> infoSysProxyUser(@Param("proxyNumber")String proxyNumber);
}
