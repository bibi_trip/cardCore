package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaOrder;
import org.xxpay.dal.dao.card.model.SaScoreFlow;

public interface SaScoreFlowMapper {
	
	int insertSaScoreFlow(SaScoreFlow saScoreFlow);
	
	int deleteSaScoreFlow(@Param("scoreFlowId")String scoreFlowId);
	
	int updateSaScoreFlow(SaScoreFlow saScoreFlow);
	
	List<SaScoreFlow> selectSaScoreFlow(SaScoreFlow saScoreFlow);
	
	int count(SaScoreFlow saScoreFlow);
	
	int totalScore(SaScoreFlow saScoreFlow);
	
	List<SaScoreFlow> infoSaScoreFlow(@Param("scoreFlowId")String scoreFlowId);
	
	int noConfigScore(@Param("userId")String userId);
	
	List<String> countUserId();
	
	long noConfigList(@Param("userId")String userId);
	
}
