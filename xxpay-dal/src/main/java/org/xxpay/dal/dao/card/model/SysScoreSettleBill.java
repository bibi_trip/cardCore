package org.xxpay.dal.dao.card.model;

import org.xxpay.dal.dao.card.FieldAnnotation;

/**
 * 
 * 积分结算账单实体类
 * @author Administrator
 *
 */
public class SysScoreSettleBill {
	
	/**
	 * 主键，账单ID
	 * 
	 */
	@FieldAnnotation(name="账单ID")
	private String billId;

	/**
	 * 账单号
	 * 
	 */
	@FieldAnnotation(name="账单号")
	private String billNumber;

	/**
	 * 账单开始日期
	 * 
	 */
	@FieldAnnotation(name="账单开始日期")
	private String billStartDate;

	/**
	 * 账单结束日期
	 * 
	 */
	@FieldAnnotation(name="账单结束日期")
	private String billEndDate;

	/**
	 * 卡号
	 * 
	 */
	@FieldAnnotation(name="卡号")
	private String cardNumber;

	/**
	 * 用户登录账号
	 * 
	 */
	@FieldAnnotation(name="用户登录账号")
	private String loginAccount;

	/**
	 * 当期结算积分
	 * 
	 */
	@FieldAnnotation(name="当期结算积分")
	private Long currentSettleScore;

	/**
	 * 出账日期
	 * 
	 */
	@FieldAnnotation(name="出账日期")
	private String billDate;

	/**
	 * 结算日期
	 * 
	 */
	@FieldAnnotation(name="结算日期")
	private String settleDate;

	/**
	 * 账单状态
	 * 
	 */
	@FieldAnnotation(name="账单状态")
	private Character billStatus;

	/**
	 * 创建时间
	 * 
	 */
	@FieldAnnotation(name="创建时间")
	private String createTime;

	/**
	 * 外键，用户ID
	 * 
	 */
	@FieldAnnotation(name="用户ID")
	private String userId;

	/**
	 * 是否删除
	 * 
	 */
	private Character isDelete;

	/**
	 * 标志
	 * 
	 */
	private Character flag;

	/**
	 * 状态
	 * 
	 */
	private Character status;

	public SysScoreSettleBill() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SysScoreSettleBill(String billId, String billNumber,
			String billStartDate, String billEndDate, String cardNumber,
			String loginAccount, Long currentSettleScore, String billDate,
			String settleDate, Character billStatus, String createTime,
			String userId, Character isDelete, Character flag, Character status) {
		super();
		this.billId = billId;
		this.billNumber = billNumber;
		this.billStartDate = billStartDate;
		this.billEndDate = billEndDate;
		this.cardNumber = cardNumber;
		this.loginAccount = loginAccount;
		this.currentSettleScore = currentSettleScore;
		this.billDate = billDate;
		this.settleDate = settleDate;
		this.billStatus = billStatus;
		this.createTime = createTime;
		this.userId = userId;
		this.isDelete = isDelete;
		this.flag = flag;
		this.status = status;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillStartDate() {
		return billStartDate;
	}

	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate;
	}

	public String getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getLoginAccount() {
		return loginAccount;
	}

	public void setLoginAccount(String loginAccount) {
		this.loginAccount = loginAccount;
	}

	public Long getCurrentSettleScore() {
		return currentSettleScore;
	}

	public void setCurrentSettleScore(Long currentSettleScore) {
		this.currentSettleScore = currentSettleScore;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}

	public Character getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(Character billStatus) {
		this.billStatus = billStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SysScoreSettleBill [billId=" + billId + ", billNumber="
				+ billNumber + ", billStartDate=" + billStartDate
				+ ", billEndDate=" + billEndDate + ", cardNumber=" + cardNumber
				+ ", loginAccount=" + loginAccount + ", currentSettleScore="
				+ currentSettleScore + ", billDate=" + billDate
				+ ", settleDate=" + settleDate + ", billStatus=" + billStatus
				+ ", createTime=" + createTime + ", userId=" + userId
				+ ", isDelete=" + isDelete + ", flag=" + flag + ", status="
				+ status + "]";
	}
	
}
