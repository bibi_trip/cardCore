package org.xxpay.dal.dao.card.model;

/**
 * 
 * 出账配置表
 * @author Administrator
 *
 */
public class SaSettleConfig {
	
	/**
	 * 主键，配置ID
	 * 
	 */
	private String configId;
	
	/**
	 * 出账配置号
	 * 
	 */
	private String configNumber;
	
	/**
	 * 清算方式
	 * 
	 */
	private Character liquidateMode;

	/**
	 * 清算日期
	 * 
	 */
	private Integer liquidateDate;

	/**
	 * 清算时间
	 * 
	 */
	private String liquidateTime;

	/**
	 * 结算方式
	 * 
	 */
	private Character settleMode;

	/**
	 * 结算日期
	 * 
	 */
	private Integer settleDate;

	/**
	 * 是否周末结算
	 * 
	 */
	private Character weekendSettle;

	/**
	 * 是否节假日结算
	 * 
	 */
	private Character holidaySettle;

	/**
	 * 创建时间
	 * 
	 */
	private String createTime;

	/**
	 * 最后更新时间
	 * 
	 */
	private String lastUpdateTime;

	/**
	 * 外键，积分卡ID
	 * 
	 */
	private String userId;

	/**
	 * 是否删除
	 * 
	 */
	private Character isDelete;

	/**
	 * 标志
	 * 
	 */
	private Character flag;

	/**
	 * 状态
	 * 
	 */
	private Character status;

	public SaSettleConfig() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SaSettleConfig(String configId, String configNumber,
			Character liquidateMode, Integer liquidateDate,
			String liquidateTime, Character settleMode, Integer settleDate,
			Character weekendSettle, Character holidaySettle,
			String createTime, String lastUpdateTime, String userId,
			Character isDelete, Character flag, Character status) {
		super();
		this.configId = configId;
		this.configNumber = configNumber;
		this.liquidateMode = liquidateMode;
		this.liquidateDate = liquidateDate;
		this.liquidateTime = liquidateTime;
		this.settleMode = settleMode;
		this.settleDate = settleDate;
		this.weekendSettle = weekendSettle;
		this.holidaySettle = holidaySettle;
		this.createTime = createTime;
		this.lastUpdateTime = lastUpdateTime;
		this.userId = userId;
		this.isDelete = isDelete;
		this.flag = flag;
		this.status = status;
	}

	public String getConfigId() {
		return configId;
	}

	public void setConfigId(String configId) {
		this.configId = configId;
	}

	public String getConfigNumber() {
		return configNumber;
	}

	public void setConfigNumber(String configNumber) {
		this.configNumber = configNumber;
	}

	public Character getLiquidateMode() {
		return liquidateMode;
	}

	public void setLiquidateMode(Character liquidateMode) {
		this.liquidateMode = liquidateMode;
	}

	public Integer getLiquidateDate() {
		return liquidateDate;
	}

	public void setLiquidateDate(Integer liquidateDate) {
		this.liquidateDate = liquidateDate;
	}

	public String getLiquidateTime() {
		return liquidateTime;
	}

	public void setLiquidateTime(String liquidateTime) {
		this.liquidateTime = liquidateTime;
	}

	public Character getSettleMode() {
		return settleMode;
	}

	public void setSettleMode(Character settleMode) {
		this.settleMode = settleMode;
	}

	public Integer getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(Integer settleDate) {
		this.settleDate = settleDate;
	}

	public Character getWeekendSettle() {
		return weekendSettle;
	}

	public void setWeekendSettle(Character weekendSettle) {
		this.weekendSettle = weekendSettle;
	}

	public Character getHolidaySettle() {
		return holidaySettle;
	}

	public void setHolidaySettle(Character holidaySettle) {
		this.holidaySettle = holidaySettle;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getuserId() {
		return userId;
	}

	public void setuserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SaSettleConfig [configId=" + configId + ", configNumber="
				+ configNumber + ", liquidateMode=" + liquidateMode
				+ ", liquidateDate=" + liquidateDate + ", liquidateTime="
				+ liquidateTime + ", settleMode=" + settleMode
				+ ", settleDate=" + settleDate + ", weekendSettle="
				+ weekendSettle + ", holidaySettle=" + holidaySettle
				+ ", createTime=" + createTime + ", lastUpdateTime="
				+ lastUpdateTime + ", userId=" + userId + ", isDelete="
				+ isDelete + ", flag=" + flag + ", status=" + status + "]";
	}
	
}
