package org.xxpay.dal.dao.card.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.xxpay.dal.dao.card.model.SaSettleConfig;

public interface SaSettleConfigMapper {
	
	List<SaSettleConfig> selectSaSettleConfig(@Param("userId")String userId);

}
