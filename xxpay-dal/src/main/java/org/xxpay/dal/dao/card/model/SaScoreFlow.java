package org.xxpay.dal.dao.card.model;

import org.xxpay.dal.dao.card.FieldAnnotation;

/**
 * 
 * 积分流水实体类
 * @author Administrator
 *
 */
public class SaScoreFlow {
	
	/**
	 * 主键，积分流水ID
	 * 
	 */
	@FieldAnnotation(name="积分流水ID")
	private String scoreFlowId;

	/**
	 * 交易记录号
	 * 
	 */
	@FieldAnnotation(name="交易记录号")
	private String dealRecordNumber;

	/**
	 * 商户订单号
	 * 
	 */
	@FieldAnnotation(name="商户订单号")
	private String merchantOrderNumber;
	
	/**
	 * 商品类型
	 * 
	 */
	@FieldAnnotation(name="商品类型")
	private Character productType;
	

	public Character getProductType() {
		return productType;
	}

	public void setProductType(Character productType) {
		this.productType = productType;
	}

	/**
	 * 出行时间
	 * 
	 */
	@FieldAnnotation(name="出行时间")
	private String tripTime;

	/**
	 * 积分流水号，自动生成。取记账时间年月时分秒+3位绝对随机数字；
	 * 收入向的流水号前缀为R；支出向的流水号前缀为C；例：C20171215125558003
	 * 
	 */
	@FieldAnnotation(name="积分流水号")
	private String scoreFlowNumber;

	/**
	 * 积分流向。值：支出0|收入1
	 * 
	 */
	@FieldAnnotation(name="积分流向")
	private Character scoreFlowDirection;

	/**
	 * 流水类型。值：收入向（收入积分0|退收积分1|积分充值2）|支出向（转出积分3|退出积分4|出账结算5）
	 * 
	 */
	@FieldAnnotation(name="流水类型")
	private Character flowType;

	/**
	 * 当次交易积分
	 * 
	 */
	@FieldAnnotation(name="当次交易积分")
	private Long currentDealScore;

	/**
	 * 剩余积分
	 * 
	 */
	@FieldAnnotation(name="剩余积分")
	private Long remainScore;

	/**
	 * 本方账号
	 * 
	 */
	@FieldAnnotation(name="本方账号")
	private String oneselfAccount;

	/**
	 * 对方账号
	 * 
	 */
	@FieldAnnotation(name="对方账号")
	private String othersAccount;

	/**
	 * 本方卡号
	 * 
	 */
	@FieldAnnotation(name="本方卡号")
	private String oneselfCardNumber;

	/**
	 * 对方卡号
	 * 
	 */
	@FieldAnnotation(name="对方卡号")
	private String othersCardNumber;

	/**
	 * 创建时间
	 * 
	 */
	@FieldAnnotation(name="创建时间")
	private String createTime;

	/**
	 * 备注。取交易记录备注
	 * 
	 */
	@FieldAnnotation(name="备注")
	private String comment;

	/**
	 * 账单号。通过是否关联订单号来判定此流水是否已出账
	 * 
	 */
	@FieldAnnotation(name="账单号")
	private String billNumber;

	/**
	 * 外键，用户ID
	 * 
	 */
	@FieldAnnotation(name="用户ID")
	private String userId;

	/**
	 * 是否删除
	 * 
	 */
	//@FieldAnnotation(name="是否删除")
	private Character isDelete;
	
	/**
	 * 标志
	 * 
	 */
	//@FieldAnnotation(name="标志")
	private Character flag;
	
	/**
	 * 状态
	 * 
	 */
	//@FieldAnnotation(name="状态")
	private Character status;

	public SaScoreFlow() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SaScoreFlow(String scoreFlowId, String dealRecordNumber,
			String merchantOrderNumber, Character productType, String tripTime,
			String scoreFlowNumber, Character scoreFlowDirection,
			Character flowType, Long currentDealScore, Long remainScore,
			String oneselfAccount, String othersAccount,
			String oneselfCardNumber, String othersCardNumber,
			String createTime, String comment, String billNumber,
			String userId, Character isDelete, Character flag, Character status) {
		super();
		this.scoreFlowId = scoreFlowId;
		this.dealRecordNumber = dealRecordNumber;
		this.merchantOrderNumber = merchantOrderNumber;
		this.productType = productType;
		this.tripTime = tripTime;
		this.scoreFlowNumber = scoreFlowNumber;
		this.scoreFlowDirection = scoreFlowDirection;
		this.flowType = flowType;
		this.currentDealScore = currentDealScore;
		this.remainScore = remainScore;
		this.oneselfAccount = oneselfAccount;
		this.othersAccount = othersAccount;
		this.oneselfCardNumber = oneselfCardNumber;
		this.othersCardNumber = othersCardNumber;
		this.createTime = createTime;
		this.comment = comment;
		this.billNumber = billNumber;
		this.userId = userId;
		this.isDelete = isDelete;
		this.flag = flag;
		this.status = status;
	}

	public String getScoreFlowId() {
		return scoreFlowId;
	}

	public void setScoreFlowId(String scoreFlowId) {
		this.scoreFlowId = scoreFlowId;
	}

	public String getDealRecordNumber() {
		return dealRecordNumber;
	}

	public void setDealRecordNumber(String dealRecordNumber) {
		this.dealRecordNumber = dealRecordNumber;
	}

	public String getMerchantOrderNumber() {
		return merchantOrderNumber;
	}

	public void setMerchantOrderNumber(String merchantOrderNumber) {
		this.merchantOrderNumber = merchantOrderNumber;
	}

	public String getTripTime() {
		return tripTime;
	}

	public void setTripTime(String tripTime) {
		this.tripTime = tripTime;
	}

	public String getScoreFlowNumber() {
		return scoreFlowNumber;
	}

	public void setScoreFlowNumber(String scoreFlowNumber) {
		this.scoreFlowNumber = scoreFlowNumber;
	}

	public Character getScoreFlowDirection() {
		return scoreFlowDirection;
	}

	public void setScoreFlowDirection(Character scoreFlowDirection) {
		this.scoreFlowDirection = scoreFlowDirection;
	}

	public Character getFlowType() {
		return flowType;
	}

	public void setFlowType(Character flowType) {
		this.flowType = flowType;
	}

	public Long getCurrentDealScore() {
		return currentDealScore;
	}

	public void setCurrentDealScore(Long currentDealScore) {
		this.currentDealScore = currentDealScore;
	}

	public Long getRemainScore() {
		return remainScore;
	}

	public void setRemainScore(Long remainScore) {
		this.remainScore = remainScore;
	}

	public String getOneselfAccount() {
		return oneselfAccount;
	}

	public void setOneselfAccount(String oneselfAccount) {
		this.oneselfAccount = oneselfAccount;
	}

	public String getOthersAccount() {
		return othersAccount;
	}

	public void setOthersAccount(String othersAccount) {
		this.othersAccount = othersAccount;
	}

	public String getOneselfCardNumber() {
		return oneselfCardNumber;
	}

	public void setOneselfCardNumber(String oneselfCardNumber) {
		this.oneselfCardNumber = oneselfCardNumber;
	}

	public String getOthersCardNumber() {
		return othersCardNumber;
	}

	public void setOthersCardNumber(String othersCardNumber) {
		this.othersCardNumber = othersCardNumber;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SaScoreFlow [scoreFlowId=" + scoreFlowId
				+ ", dealRecordNumber=" + dealRecordNumber
				+ ", merchantOrderNumber=" + merchantOrderNumber
				+ ", tripTime=" + tripTime + ", scoreFlowNumber="
				+ scoreFlowNumber + ", scoreFlowDirection="
				+ scoreFlowDirection + ", flowType=" + flowType
				+ ", currentDealScore=" + currentDealScore + ", remainScore="
				+ remainScore + ", oneselfAccount=" + oneselfAccount
				+ ", othersAccount=" + othersAccount + ", oneselfCardNumber="
				+ oneselfCardNumber + ", othersCardNumber=" + othersCardNumber
				+ ", createTime=" + createTime + ", comment=" + comment
				+ ", billNumber=" + billNumber + ", userId=" + userId
				+ ", isDelete=" + isDelete + ", flag=" + flag + ", status="
				+ status + "]";
	}
	
}
