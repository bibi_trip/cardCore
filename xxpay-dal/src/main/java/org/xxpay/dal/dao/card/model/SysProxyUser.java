package org.xxpay.dal.dao.card.model;

/**
 * 
 * 代理用户实体类
 * @author Administrator
 *
 */
public class SysProxyUser {
	
	/**
	 * 主键，代理用户ID
	 * 
	 */
	private String proxyId;
	
	/**
	 * 代理号
	 * 
	 */
	private String proxyNumber;
	
	/**
	 * 代理类型
	 * 
	 */
	private Character proxyType;
	
	/**
	 * 代理名称
	 * 
	 */
	private String proxyName;
	
	/**
	 * 企业|店铺名称
	 * 
	 */
	private String companyName;
	
	/**
	 * 企业|店铺座机
	 * 
	 */
	private String companyPhone;
	
	/**
	 * 办公驻址
	 * 
	 */
	private String officeAddr;
	
	/**
	 * 联系人
	 * 
	 */
	private String contact;
	
	/**
	 * 联系人手机号码
	 * 
	 */
	private String contactPhone;
	
	/**
	 * 联系人电子邮箱
	 * 
	 */
	private String contactEmail;
	
	/**
	 * 注册附件路径
	 * 
	 */
	private String regAttachment;
	
	/**
	 * 外键，用户ID
	 * 
	 */
	private String userId;
	
	/**
	 * 是否删除
	 * 
	 */
	private Character isDelete;
	
	/**
	 * 标志
	 * 
	 */
	private Character flag;
	
	/**
	 * 状态
	 * 
	 */
	private Character status;

	public SysProxyUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SysProxyUser(String proxyId, String proxyNumber, Character proxyType,
			String proxyName, String companyName, String companyPhone,
			String officeAddr, String contact, String contactPhone,
			String contactEmail, String regAttachment, String userId,
			Character isDelete, Character flag, Character status) {
		super();
		this.proxyId = proxyId;
		this.proxyNumber = proxyNumber;
		this.proxyType = proxyType;
		this.proxyName = proxyName;
		this.companyName = companyName;
		this.companyPhone = companyPhone;
		this.officeAddr = officeAddr;
		this.contact = contact;
		this.contactPhone = contactPhone;
		this.contactEmail = contactEmail;
		this.regAttachment = regAttachment;
		this.userId = userId;
		this.isDelete = isDelete;
		this.flag = flag;
		this.status = status;
	}

	public String getProxyId() {
		return proxyId;
	}

	public void setProxyId(String proxyId) {
		this.proxyId = proxyId;
	}

	public String getProxyNumber() {
		return proxyNumber;
	}

	public void setProxyNumber(String proxyNumber) {
		this.proxyNumber = proxyNumber;
	}

	public Character getProxyType() {
		return proxyType;
	}

	public void setProxyType(Character proxyType) {
		this.proxyType = proxyType;
	}

	public String getProxyName() {
		return proxyName;
	}

	public void setProxyName(String proxyName) {
		this.proxyName = proxyName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getOfficeAddr() {
		return officeAddr;
	}

	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getRegAttachment() {
		return regAttachment;
	}

	public void setRegAttachment(String regAttachment) {
		this.regAttachment = regAttachment;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

	public Character getFlag() {
		return flag;
	}

	public void setFlag(Character flag) {
		this.flag = flag;
	}

	public Character getStatus() {
		return status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SysProxyUser [proxyId=" + proxyId + ", proxyNumber="
				+ proxyNumber + ", proxyType=" + proxyType + ", proxyName="
				+ proxyName + ", companyName=" + companyName
				+ ", companyPhone=" + companyPhone + ", officeAddr="
				+ officeAddr + ", contact=" + contact + ", contactPhone="
				+ contactPhone + ", contactEmail=" + contactEmail
				+ ", regAttachment=" + regAttachment + ", userId=" + userId
				+ ", isDelete=" + isDelete + ", flag=" + flag + ", status="
				+ status + "]";
	}
	
}
